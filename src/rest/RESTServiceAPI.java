package rest;

import rest.request.RequestClass;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(RESTServiceAPI.SERVICE_PATH)
public interface RESTServiceAPI {
    String SERVER_URI = "https://0.0.0.0/";

    String SERVICE_PATH = "/service";

    String CREATE_MONEY_PATH = "/create";
    String TRANSFER_MONEY_PATH = "/transfer";
    String TRANSFER_MONEY_NO_LIMIT_PATH = "/transfer_no_limit";
    String CURRENT_AMOUNT_PATH = "/current_amount";
    String CURRENT_AMOUNT_BETWEEN_PATH = "/current_amount_between";
    String ADD_DOUBLE_PATH = "/add_double";
    String SUB_DOUBLE_PATH = "/sub_double";

    String FLUSH_PATH = "/flush";
    String PING_PATH = "/ping";
    String STOP_PATH = "/stop";

    String PUT_OPE_PATH = "/put_ope";
    String GET_OPE_PATH = "/get_ope";
    String GET_OPE_BETWEEN_PATH = "/get_ope_between";
    String ADD_OPE_PATH = "/add_ope";
    String SUB_OPE_PATH = "/sub_ope";

    String PUT_SUM_PATH = "/put_sum";
    String GET_SUM_PATH = "/get_sum";
    String SUB_SUM_PATH = "/sub_sum";
    String ADD_SUM_PATH = "/add_sum";
    String GET_SUM_BETWEEN_PATH = "/get_sum_between";

    String COND_UPDATE = "/cond_update";

    String CREATE_AANY_PATH = "/create_any";

    @POST
    @Path(RESTServiceAPI.CREATE_AANY_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response createAny(RequestClass request);

    @POST
    @Path(RESTServiceAPI.CREATE_MONEY_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response createMoney(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.TRANSFER_MONEY_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response transferMoney(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.TRANSFER_MONEY_NO_LIMIT_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response transferMoneyNoLimit(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.CURRENT_AMOUNT_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response currentAmount(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.CURRENT_AMOUNT_BETWEEN_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response currentAmountBetween(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.ADD_DOUBLE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response addDouble(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.SUB_DOUBLE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response subDouble(RequestClass request);

    @POST
    @Path(RESTServiceAPI.PUT_OPE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response putOPE(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.GET_OPE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getOPE(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.GET_OPE_BETWEEN_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getOPEBetween(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.ADD_OPE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response addOPE(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.SUB_OPE_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response subOPE(RequestClass request);

    @POST
    @Path(RESTServiceAPI.PUT_SUM_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response putSum(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.GET_SUM_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getSum(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.ADD_SUM_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response addSum(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.SUB_SUM_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response subSum(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.GET_SUM_BETWEEN_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getSumBetween(RequestClass request);

    @PUT
    @Path(RESTServiceAPI.COND_UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response condUpdate(RequestClass request);


    @GET
    @Path(RESTServiceAPI.FLUSH_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    Response flush();

    @GET
    @Path(RESTServiceAPI.PING_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    Response ping();

    @GET
    @Path(RESTServiceAPI.STOP_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    Response stop();
}
