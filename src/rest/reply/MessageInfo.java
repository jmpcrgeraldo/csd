package rest.reply;

import bftsmart.tom.core.messages.TOMMessage;

public class MessageInfo {

    public byte[] serializedMessage,
            serializedMessageMAC,
            serializedMessageSignature,
            content;

    public int id;

    public MessageInfo() {

    }

    public MessageInfo(TOMMessage messages) {
        this.serializedMessage = messages.serializedMessage;
        this.serializedMessageMAC = messages.serializedMessageMAC;
        this.serializedMessageSignature = messages.serializedMessageSignature;
        this.content = messages.getContent();
        this.id = messages.getSender();
    }

}
