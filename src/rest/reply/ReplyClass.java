package rest.reply;

import rest.ServerRequestType;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

public class ReplyClass implements Serializable {

    public ServerRequestType request;
    public String user_key;
    public long nonce;
    public int status;
    public Double value;
    public Long opeValue;
    public List<String> keyList;
    public BigInteger sumInt;

    public ReplyClass() {

    }


    public ReplyClass(int status, long nonce, String user_key, ServerRequestType request) {
        this.user_key = user_key;
        this.nonce = nonce;
        this.status = status;
        this.request = request;
    }

    public Object getValue() {

        if(value != null)
            return value;

        if(opeValue != null)
            return opeValue;

        if(sumInt != null)
            return sumInt;

        if(keyList != null)
            return keyList;


        return null;
    }

    public ReplyClass(int status, long nonce, String user_key, List<String> keyList, ServerRequestType request) {
        this(status, nonce, user_key, request);
        this.keyList = keyList;
    }

    public ReplyClass(int status, long nonce, String user_key, Long opeValue, ServerRequestType request) {
        this(status, nonce, user_key, request);
        this.opeValue = opeValue;
    }

    public ReplyClass(int status, long nonce, String user_key, Double value, ServerRequestType request) {
        this(status, nonce, user_key, request);
        this.value = value;
    }

    public ReplyClass(int status, long nonce, String user_key, BigInteger sumIntValue, ServerRequestType request) {
        this(status, nonce, user_key, request);
        this.sumInt = sumIntValue;
    }
}
