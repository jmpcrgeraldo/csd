package rest.server;

import bftsmart.tom.ServiceProxy;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.util.Extractor;
import rest.RESTServiceAPI;
import rest.ServerRequestType;
import rest.reply.MessageInfo;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import tools.Config;
import tools.Conversion;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class RESTServer implements RESTServiceAPI {
    private static final Logger LOG = Logger.getLogger(RESTServer.class.getName());
    private ServiceProxy serviceProxy;
    private MessageInfo[] myTomMessages = new MessageInfo[3 * Config.getProperty(Config.PROPS.client_max_numb_faults) + 1];
    private BFTClient bftClient;

    public RESTServer(int id) {

        this.bftClient = new BFTClient(id);

        Extractor extractor = (tomMessages, sameContent, lastReceived) -> {
            myTomMessages = convertToMsgInfo(tomMessages);
            return tomMessages[lastReceived];
        };

        this.serviceProxy = new ServiceProxy(id, "config", null, extractor);
    }

    @Override
    public Response createAny(RequestClass request) {
        LOG.fine("GOT CREATE ANY");

        return handleRequest(request, ServerRequestType.CREATE_ANY, true);
    }

    @Override
    public Response createMoney(RequestClass request) {
        LOG.fine("GOT CREATE MONEY");

        return handleRequest(request, ServerRequestType.CREATE_MONEY, true);
    }

    @Override
    public Response transferMoney(RequestClass request) {
        LOG.fine("GOT TRANSFER MONEY");

        return handleRequest(request, ServerRequestType.TRANSFER_MONEY, true);
    }

    @Override
    public Response transferMoneyNoLimit(RequestClass request) {
        LOG.fine("GOT TRANSFER MONEY NO LIMIT");

        return handleRequest(request, ServerRequestType.TRANSFER_MONEY_NO_LIMIT, true);
    }

    @Override
    public Response currentAmount(RequestClass request) {
        LOG.fine("GOT CURRENT MONEY");

        return handleRequest(request, ServerRequestType.CURRENT_AMOUNT, false);
    }

    @Override
    public Response currentAmountBetween(RequestClass request) {
        LOG.fine("GOT CURRENT AMOUNT BETWEEN");

        return handleRequest(request, ServerRequestType.CURRENT_AMOUNT_BETWEEN, false);
    }

    @Override
    public Response addDouble(RequestClass request) {
        LOG.fine("GOT ADD DOUBLE");

        return handleRequest(request, ServerRequestType.ADD_DOUBLE, true);
    }

    @Override
    public Response subDouble(RequestClass request) {
        LOG.fine("GOT SUB DOUBLE");

        return handleRequest(request, ServerRequestType.SUB_DOUBLE, true);
    }

    @Override
    public Response getOPE(RequestClass request) {
        LOG.fine("GOT GET OPE");
        return handleRequest(request, ServerRequestType.GET_OPE, false);
    }

    @Override
    public Response putOPE(RequestClass request) {
        LOG.fine("GOT PUT OPE");

        return handleRequest(request, ServerRequestType.PUT_OPE, true);
    }

    public Response getOPEBetween(RequestClass request) {
        LOG.fine("GOT GET OPE BETWEEN");

        return handleRequest(request, ServerRequestType.GET_OPE_BETWEEN, false);
    }

    @Override
    public Response addOPE(RequestClass request) {
        LOG.fine("GOT ADD OPE");

        return handleRequest(request, ServerRequestType.ADD_OPE, true);
    }

    @Override
    public Response subOPE(RequestClass request) {
        LOG.fine("GOT SUB OPE");

        return handleRequest(request, ServerRequestType.SUB_OPE, true);
    }

    @Override
    public Response putSum(RequestClass request) {
        LOG.fine("GOT PUT SUM");

        return handleRequest(request, ServerRequestType.PUT_SUM_INT, true);
    }

    @Override
    public Response getSum(RequestClass request) {
        LOG.fine("GOT GET SUM");

        return handleRequest(request, ServerRequestType.GET_SUM_INT, false);
    }

    @Override
    public Response addSum(RequestClass request) {
        LOG.fine("GOT ADD_SUM_INT SUM");

        return handleRequest(request, ServerRequestType.ADD_SUM_INT, true);
    }

    @Override
    public Response subSum(RequestClass request) {
        LOG.fine("GOT SUB_SUM_INT SUM");

        return handleRequest(request, ServerRequestType.SUB_SUM_INT, true);
    }

    @Override
    public Response getSumBetween(RequestClass request) {
        LOG.fine("GOT GET SUM BETWEEN");

        return handleRequest(request, ServerRequestType.GET_SUM_BETWEEN, false);
    }

    @Override
    public Response condUpdate(RequestClass request) {
        LOG.fine("GOT CONDITIONAL UPDATE");

        return handleRequest(request, ServerRequestType.COND_UPDATE, true);
    }

    @Override
    public Response flush() {
        LOG.warning("---GOT FLUSH---");

        this.bftClient.flush();
        return Response.ok().build();
    }

    @Override
    public Response ping() {
        LOG.fine("---GOT PING---");
        return Response.ok().build();
    }

    @Override
    public Response stop() {
        Timer timer = new Timer();

        TimerTask delayedThreadStartTask = new TimerTask() {
            @Override
            public void run() {
                new Thread(() -> {

                    System.err.println("_______________________________________________________________EXITING");
                    //System.exit(-1);
                }).start();
            }
        };

        timer.schedule(delayedThreadStartTask, 60 * 1000);
        return Response.ok().build();
    }

    private Response handleRequest(RequestClass request, ServerRequestType requestType, boolean ordered) {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

            objOut.writeObject(requestType);
            objOut.writeObject(request);

            objOut.flush();
            byteOut.flush();

            byte[] reply;

            if (ordered) {
                reply = serviceProxy.invokeOrdered(byteOut.toByteArray());
            } else {
                reply = serviceProxy.invokeUnordered(byteOut.toByteArray());
            }

            ReplyClass rc = Conversion.getReplyObject(reply);

            if (rc == null) {
                LOG.severe("Error reading object: Object is NULL");
                return Response.status(Response.Status.CONFLICT).entity(null).build();
                //throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);//INTERNAL_SERVER_ERROR
            }

            return Response.status(rc.status).entity(myTomMessages).build();

        } catch (IOException e) {
            e.printStackTrace();
            LOG.severe("Error reading object: IOException");
            return Response.status(Response.Status.CONFLICT).entity(null).build();
            //throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    private MessageInfo[] convertToMsgInfo(TOMMessage[] messages) {
        int c = 0;

        for (TOMMessage msg : messages)
            if (msg == null)
                c++;

        if (c > 1) System.exit(-1);

        MessageInfo[] msgInfo = new MessageInfo[messages.length];

        for (int i = 0; i < messages.length; i++) {
            if (messages[i] != null)
                msgInfo[i] = new MessageInfo(messages[i]);
        }

        return msgInfo;
    }


}
