package rest.server.handlers;

import container.ErrorExc;
import container.SconeClient;
import rest.ServerRequestType;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import tools.Signatures;
import tools.WrapperMap;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.ObjectOutput;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OPEHandler {

    public static void handlePutOPE(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        Long amount = request.getReqLong1();
        long nonce = request.getNonce();

        // TODO move common validation to separate module
        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) { //|| amount <= 0) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + amount + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else { // if (db.getOPEInt(content_key) == null) {
            db.addOPEInt(request.getPubKey() + content_key, amount); // PREFIX : request.getPubKey()
            st = Response.Status.OK;
        }
        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, amount, ServerRequestType.PUT_OPE);

        objOut.writeObject(rc);
    }

    public static void handleGetOPE(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        Long amount = null;
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            amount = db.getOPEInt(request.getPubKey() + content_key);

            if (amount == null) {
                st = Response.Status.NOT_FOUND;
            } else {
                st = Response.Status.OK;
            }

        }
        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, amount, ServerRequestType.GET_OPE);
/*

        if (is_byzantine && new Random().nextInt(10) == 0) {
            rc = new ReplyClass(0, 0, "", 0d, ServerRequestType.CURRENT_AMOUNT);
        }

*/
        objOut.writeObject(rc);
    }

    public static void handleGetOPEBetween(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String contentKey = request.getReqString1();

        long lowerValue = request.getReqLong1();
        long upperValue = request.getReqLong2();

        List<String> keysInRange = new LinkedList<>();
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), contentKey + lowerValue + upperValue + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            for (Map.Entry<String, Long> entry : db.getOPEEntries())
                if (entry.getKey().startsWith(contentKey))
                    if (entry.getValue() >= lowerValue && entry.getValue() <= upperValue)
                        keysInRange.add(entry.getKey().replace(contentKey, ""));

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), contentKey, keysInRange, ServerRequestType.GET_OPE_BETWEEN);
/*

        if (is_byzantine && new Random().nextInt(10) == 0) {
            rc = new ReplyClass(0, 0, "", 0d, ServerRequestType.CURRENT_AMOUNT);
        }

*/
        objOut.writeObject(rc);
    }

    public static void handleAddOPE(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        String userKey = request.getReqString2();
        String symKey = request.getReqString3();

        Long existingValue = db.getOPEInt(request.getPubKey() + content_key);
        Long amountToAdd = request.getReqLong1();
        long nonce = request.getNonce();

        long res = -1L;

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
            System.out.println(1);
        } else if (content_key == null) { //|| amount <= 0) {
            st = Response.Status.BAD_REQUEST;
            System.out.println(2);
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + amountToAdd + userKey + symKey + nonce)) {
            st = Response.Status.BAD_REQUEST;
            System.out.println(3);
        } else { // if (db.getOPEInt(content_key) == null) {
            try {
                res = SconeClient.exec_ope_add(existingValue, amountToAdd, userKey, symKey);
                st = Response.Status.OK;
            } catch (Exception errorExc) {
                st = Response.Status.PRECONDITION_FAILED;
                System.err.println("Precond Failed");
            }
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, res, ServerRequestType.ADD_OPE);

        objOut.writeObject(rc);
    }

    public static void handleSubOPE(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        String userKey = request.getReqString2();
        String symKey = request.getReqString3();

        Long existingValue = db.getOPEInt(request.getPubKey() + content_key);
        Long amountToSub = request.getReqLong1();
        long nonce = request.getNonce();

        Long res = null;

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + amountToSub + userKey + symKey + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {

            try {
                res = SconeClient.exec_ope_sub(existingValue, amountToSub, userKey, symKey);
                st = Response.Status.OK;
                rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, res, ServerRequestType.SUB_OPE);

                objOut.writeObject(rc);
                return;
            } catch (ErrorExc errorExc) {
                st = Response.Status.PRECONDITION_FAILED;
                System.err.println("Could not Exec OPE Sub");

            }
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, -1L, ServerRequestType.SUB_OPE);
        objOut.writeObject(rc);
    }
}
