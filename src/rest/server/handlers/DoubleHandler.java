package rest.server.handlers;

import hlib.hj.mlib.HomoAdd;
import rest.ServerRequestType;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import tools.Signatures;
import tools.WrapperMap;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.ObjectOutput;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DoubleHandler {

    public static void handleCreateMoney(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        double amount = request.getReqDouble1();
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
            System.out.println("NONCE");
        } else if (content_key == null || amount <= 0) {
            st = Response.Status.BAD_REQUEST;
            System.out.println("COntent " + (content_key == null) + "Amo: " + amount);
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + amount + nonce)) {
            st = Response.Status.BAD_REQUEST;
            System.out.println("SINCA");
        } else { // if (db.getDouble(content_key) == null) {
            db.addDouble(request.getPubKey() + content_key, amount);
            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, amount, ServerRequestType.CREATE_MONEY);

        objOut.writeObject(rc);
    }

    public static void handleCurrentAmount(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        Double amount = null;
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            amount = db.getDouble(request.getPubKey() + content_key);

            if (amount == null) {
                st = Response.Status.NOT_FOUND;
            } else {
                st = Response.Status.OK;
            }

        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, amount, ServerRequestType.CURRENT_AMOUNT);

        objOut.writeObject(rc);
    }

    public static void handleCurrentAmountBetween(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        double lowerValue = request.getReqDouble1();
        double upperValue = request.getReqDouble2();

        List<String> keysInRange = new LinkedList<>();
        long nonce = request.getNonce();
        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), "" + lowerValue + upperValue + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            for (Map.Entry<String, Double> entry : db.getDoubleEntries())
                if(entry.getKey().startsWith(request.getPubKey()))
                    if (entry.getValue() >= lowerValue && entry.getValue() <= upperValue)
                        keysInRange.add(entry.getKey().replace(request.getPubKey(), ""));

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), request.getPubKey(), keysInRange, ServerRequestType.CURRENT_AMOUNT_BETWEEN);
/*

        if (is_byzantine && new Random().nextInt(10) == 0) {
            rc = new ReplyClass(0, 0, "", 0d, ServerRequestType.CURRENT_AMOUNT);
        }

*/
        objOut.writeObject(rc);
    }

    public static void handleTransferMoney(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {

        Response.Status st;
        ReplyClass rc;

        String orig_content_key = request.getReqString1();
        String dest_content_key = request.getReqString2();
        long nonce = request.getNonce();

        double amount = request.getReqDouble1();

        Double user_orig_balance;

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (orig_content_key == null || orig_content_key.equals(dest_content_key)) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), orig_content_key + dest_content_key + amount + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            user_orig_balance = db.getDouble(request.getPubKey() + orig_content_key);

            if (user_orig_balance < amount) {
                st = Response.Status.BAD_REQUEST;
            } else {

                if (user_orig_balance - amount == 0) {
                    db.removeDouble(request.getPubKey() + orig_content_key);
                } else {
                    db.addDouble(request.getPubKey() + orig_content_key, user_orig_balance - amount);
                }

                Double dest_amount = db.getDouble(request.getPubKey() + dest_content_key);
                if (dest_amount == null) {
                    db.addDouble(request.getPubKey() + dest_content_key, amount);
                } else {
                    db.addDouble(request.getPubKey() + dest_content_key, dest_amount + amount);
                }

                st = Response.Status.OK;
            }
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), orig_content_key, amount, ServerRequestType.TRANSFER_MONEY);

        objOut.writeObject(rc);
    }

    public static void handleTransferMoneyNoLimit(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {

        Response.Status st;
        ReplyClass rc;

        String orig_content_key = request.getReqString1();
        String dest_content_key = request.getReqString2();
        long nonce = request.getNonce();

        double amount = request.getReqDouble1();

        Double user_orig_balance;

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (orig_content_key == null || orig_content_key.equals(dest_content_key)) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), orig_content_key + dest_content_key + amount + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {

            user_orig_balance = db.getDouble(request.getPubKey() + orig_content_key);
            db.addDouble(request.getPubKey() + orig_content_key, user_orig_balance - amount);

            Double dest_amount = db.getDouble(request.getPubKey() + dest_content_key);
            db.addDouble(request.getPubKey() + dest_content_key, dest_amount + amount);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), orig_content_key, amount, ServerRequestType.TRANSFER_MONEY_NO_LIMIT);

        objOut.writeObject(rc);
    }

    public static void handleAddDouble(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        Double valueToAdd = request.getReqDouble1();
        Double existingValue = db.getDouble(request.getPubKey() + content_key);
        Double addResult = null;

        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            System.out.println("NONCE");
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            System.out.println("CONTENT KEY");
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + valueToAdd + nonce)) {
            System.out.println("SIGN");
            st = Response.Status.BAD_REQUEST;
        } else if (existingValue == null) {
            st = Response.Status.NOT_FOUND;
        } else {
            addResult = existingValue + valueToAdd;
            db.addDouble(request.getPubKey() + content_key, addResult);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, addResult != null ? addResult : valueToAdd, ServerRequestType.ADD_DOUBLE);

        objOut.writeObject(rc);
    }

    public static void handleSubDouble(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        Double valueToSub = request.getReqDouble1();
        Double existingValue = db.getDouble(request.getPubKey() + content_key);
        Double subResult = null;

        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + valueToSub + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (existingValue == null) {
            st = Response.Status.NOT_FOUND;
        } else {
            subResult = existingValue - valueToSub;
            db.addDouble(request.getPubKey() + content_key, subResult);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, subResult != null ? subResult : valueToSub, ServerRequestType.SUB_DOUBLE);

        objOut.writeObject(rc);
    }
}
