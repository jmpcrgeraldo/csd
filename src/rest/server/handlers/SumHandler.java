package rest.server.handlers;

import container.SconeClient;
import hlib.hj.mlib.HomoAdd;
import rest.ServerRequestType;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import tools.Signatures;
import tools.WrapperMap;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.ObjectOutput;
import java.math.BigInteger;
import java.util.*;

public class SumHandler {

    public static void handlePutSumInt(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        BigInteger value = request.getBigInt1();
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + value + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            db.addSumInt(request.getPubKey() + content_key, value);
            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, value, ServerRequestType.PUT_SUM_INT);

        objOut.writeObject(rc);
    }

    public static void handleGetSumInt(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        BigInteger amount = null;
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            amount = db.getSumInt(request.getPubKey() + content_key);

            if (amount == null) {
                st = Response.Status.NOT_FOUND;
            } else {
                st = Response.Status.OK;
            }
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, amount, ServerRequestType.GET_SUM_INT);

        objOut.writeObject(rc);
    }

    public static void handleAddSumInt(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        BigInteger valueToAdd = request.getBigInt1();
        BigInteger nSquare = request.getNSquare();
        BigInteger existingValue = db.getSumInt(request.getPubKey() + content_key);
        BigInteger addResult = null;

        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + valueToAdd + nSquare + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (existingValue == null) {
            st = Response.Status.NOT_FOUND;
        } else {
            addResult = HomoAdd.sum(existingValue, valueToAdd, nSquare);
            db.addSumInt(request.getPubKey() + content_key, addResult);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, addResult != null ? addResult : valueToAdd, ServerRequestType.ADD_SUM_INT);

        objOut.writeObject(rc);
    }

    public static void handleSubSumInt(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();
        BigInteger valueToSub = request.getBigInt1();
        BigInteger nSquare = request.getNSquare();
        BigInteger existingValue = db.getSumInt(request.getPubKey() + content_key);
        BigInteger subResult = null;

        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (content_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + valueToSub + nSquare + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (existingValue == null) {
            st = Response.Status.NOT_FOUND;
        } else {
            subResult = HomoAdd.dif(existingValue, valueToSub, nSquare);
            db.addSumInt(request.getPubKey() + content_key, subResult);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, subResult != null ? subResult : valueToSub, ServerRequestType.SUB_SUM_INT);

        objOut.writeObject(rc);
    }

    public static void handleGetSumIntBetween(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st;
        ReplyClass rc;

        String content_key = request.getReqString1();

        String userKey = request.getReqString2();
        String symKey = request.getReqString3();

        BigInteger lowerValue = request.getBigInt1();
        BigInteger upperValue = request.getBigInt2();

        long nonce = request.getNonce();

        List<Map.Entry<String, BigInteger>> entries = new LinkedList<>();

        List<String> res = null;

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), content_key + lowerValue + upperValue + userKey + symKey + nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else {
            for (Map.Entry<String, BigInteger> entry : db.getSumEntries()) {
                if (entry.getKey().startsWith(content_key)) {
                    entries.add(
                            new AbstractMap.SimpleEntry<>(
                                    entry.getKey().replace(content_key, ""),
                                    entry.getValue()));
                }
            }

            res = SconeClient.exec_homo_relations(lowerValue, upperValue, entries, userKey, symKey);

            st = Response.Status.OK;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), content_key, res, ServerRequestType.GET_SUM_BETWEEN);

        objOut.writeObject(rc);
    }
}
