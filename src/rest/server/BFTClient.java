package rest.server;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.defaultservices.DefaultSingleRecoverable;
import container.ErrorExc;
import container.SconeClient;
import rest.ServerRequestType;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import rest.request.UpdateOP;
import rest.server.handlers.DoubleHandler;
import rest.server.handlers.OPEHandler;
import rest.server.handlers.SumHandler;
import tools.Config;
import tools.Signatures;
import tools.WrapperMap;

import javax.ws.rs.core.Response;
import java.io.*;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class BFTClient extends DefaultSingleRecoverable {

    private static final int MAX_NUM_NONCES = 50000;
    private static final Logger LOG = Logger.getLogger(BFTClient.class.getName());
    private Set<Long> previousNonces;
    private WrapperMap db;

    public BFTClient(int id) {
        boolean is_byzantine = Config.getProperty(Config.PROPS.server_byzantine) == 1 && id == 0;

        this.db = new WrapperMap();

        this.previousNonces = ConcurrentHashMap.newKeySet(MAX_NUM_NONCES);

        new ServiceReplica(id, this, this);

        LOG.info("BFT Client running with id: " + id);

        if (is_byzantine)
            LOG.severe("--------------------------------------This server is byzantine");
    }

    public void flush() {
        this.db.clear();
        this.previousNonces.clear();
    }

    @Override
    public byte[] getSnapshot() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {
            objOut.writeObject(db);
            return byteOut.toByteArray();
        } catch (IOException e) {
            LOG.severe("Error while taking snapshot : " + e);
        }
        return new byte[0];
    }


    @Override
    public void installSnapshot(byte[] state) {
        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(state);
             ObjectInput objIn = new ObjectInputStream(byteIn)) {
            db = (WrapperMap) objIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            LOG.severe("Error while installing snapshot : " + e);
        }
    }

    @Override
    public byte[] appExecuteOrdered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

            ServerRequestType reqType = (ServerRequestType) objIn.readObject();
            RequestClass request = (RequestClass) objIn.readObject();
            boolean hasReply = true;

            switch (reqType) {
                case CREATE_MONEY:
                    DoubleHandler.handleCreateMoney(this.db, this.previousNonces, objOut, request);
                    break;
                case CURRENT_AMOUNT:
                    DoubleHandler.handleCurrentAmount(this.db, this.previousNonces, objOut, request);
                    break;
                case CURRENT_AMOUNT_BETWEEN:
                    DoubleHandler.handleCurrentAmountBetween(this.db, this.previousNonces, objOut, request);
                    break;
                case TRANSFER_MONEY:
                    DoubleHandler.handleTransferMoney(this.db, this.previousNonces, objOut, request);
                    break;
                case TRANSFER_MONEY_NO_LIMIT:
                    DoubleHandler.handleTransferMoneyNoLimit(this.db, this.previousNonces, objOut, request);
                    break;
                case ADD_DOUBLE:
                    DoubleHandler.handleAddDouble(this.db, this.previousNonces, objOut, request);
                    break;
                case SUB_DOUBLE:
                    DoubleHandler.handleSubDouble(this.db, this.previousNonces, objOut, request);
                    break;
                case PUT_OPE:
                    OPEHandler.handlePutOPE(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_OPE:
                    OPEHandler.handleGetOPE(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_OPE_BETWEEN:
                    OPEHandler.handleGetOPEBetween(this.db, this.previousNonces, objOut, request);
                    break;
                case ADD_OPE:
                    OPEHandler.handleAddOPE(this.db, this.previousNonces, objOut, request);
                    break;
                case SUB_OPE:
                    OPEHandler.handleSubOPE(this.db, this.previousNonces, objOut, request);
                    break;
                case ADD_SUM_INT:
                    SumHandler.handleAddSumInt(this.db, this.previousNonces, objOut, request);
                    break;
                case SUB_SUM_INT:
                    SumHandler.handleSubSumInt(this.db, this.previousNonces, objOut, request);
                    break;
                case PUT_SUM_INT:
                    SumHandler.handlePutSumInt(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_SUM_INT:
                    SumHandler.handleGetSumInt(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_SUM_BETWEEN:
                    SumHandler.handleGetSumIntBetween(this.db, this.previousNonces, objOut, request);
                    break;
                case COND_UPDATE:
                    handleCondUpdate(this.db, this.previousNonces, objOut, request);
                    break;
                case CREATE_ANY:
                    handleCreateAny(this.db, this.previousNonces, objOut, request);
                    break;
                default:
                    hasReply = false;
                    break;
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            }

        } catch (IOException | ClassNotFoundException e) {
            LOG.severe("Error while executing ordered : " + e);
        }
        return reply;
    }

    @Override
    public byte[] appExecuteUnordered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

            ServerRequestType reqType = (ServerRequestType) objIn.readObject();
            RequestClass request = (RequestClass) objIn.readObject();
            boolean hasReply = true;

            switch (reqType) {
                case CURRENT_AMOUNT:
                    DoubleHandler.handleCurrentAmount(this.db, this.previousNonces, objOut, request);
                    break;
                case CURRENT_AMOUNT_BETWEEN:
                    DoubleHandler.handleCurrentAmountBetween(this.db, this.previousNonces, objOut, request);
                case GET_OPE:
                    OPEHandler.handleGetOPE(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_OPE_BETWEEN:
                    OPEHandler.handleGetOPEBetween(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_SUM_INT:
                    SumHandler.handleGetSumInt(this.db, this.previousNonces, objOut, request);
                    break;
                case GET_SUM_BETWEEN:
                    SumHandler.handleGetSumIntBetween(this.db, this.previousNonces, objOut, request);
                    break;
                default:
                    hasReply = false;
                    break;
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            }

        } catch (IOException | ClassNotFoundException e) {
            LOG.severe("Error while executing unordered : " + e);
        }

        return reply;
    }

    private void handleCreateAny(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        switch (request.getReqType()) {
            case DOUBLE:
                DoubleHandler.handleCreateMoney(db, previousNonces, objOut, request);
                break;
            case OPE_INT:
                OPEHandler.handlePutOPE(db, previousNonces, objOut, request);
                break;
            case SUM_INT:
                SumHandler.handlePutSumInt(db, previousNonces, objOut, request);
                break;
            default:
                System.err.println("Error in Handle Create Any");
                break;
        }
    }

    public void handleCondUpdate(WrapperMap db, Set<Long> previousNonces, ObjectOutput objOut, RequestClass request) throws IOException {
        Response.Status st = null;
        ReplyClass rc;

        String cond_key = request.getReqString1();
        Object tmp_cond_val = request.getReqObject();
        int cond = (int) request.getReqLong1();
        List<UpdateOP> list = request.getReqList();
        WrapperMap.TYPE type = request.getReqType();
        String user_key = request.getReqString2();
        String sym_key = request.getReqString3();
        long nonce = request.getNonce();

        if (!previousNonces.add(nonce)) {
            st = Response.Status.BAD_REQUEST;
        } else if (cond_key == null || tmp_cond_val == null || list == null || user_key == null) {
            st = Response.Status.BAD_REQUEST;
        } else if (!Signatures.check_sign(request.getSignature(), request.getPubKey(), cond_key + tmp_cond_val + cond + list + type + user_key + sym_key + nonce)) {
            st = Response.Status.BAD_REQUEST;
        }

        boolean res = false;
        switch (type) {
            case DOUBLE:
                res = execDouble(cond_key, tmp_cond_val, cond, list);
                break;
            case OPE_INT:
                res = execOPE(cond_key, tmp_cond_val, cond, list, user_key, sym_key);
                break;
            case SUM_INT:
                res = execSUM(cond_key, tmp_cond_val, cond, list, user_key, sym_key);
                break;
        }

        if (st == null) {
            if (res) st = Response.Status.OK;
            else st = Response.Status.PRECONDITION_FAILED;
        }

        rc = new ReplyClass(st.getStatusCode(), request.getNonce(), cond_key, ServerRequestType.COND_UPDATE);

        objOut.writeObject(rc);
    }

    private boolean execDouble(String cond_key, Object tmp_cond_val, int cond, List<UpdateOP> list) {
        Double val = db.getDouble(cond_key);
        Double cond_val = (Double) tmp_cond_val;

        int result = val.compareTo(cond_val);

        boolean check = checkResult(result, cond);

        if (check) {
            for (UpdateOP updateOp : list) {
                String updateKey = updateOp.getUpdateKey();
                Double updateValue = updateOp.getUpdateValueD();

                if (updateOp.getOp() == 0) {
                    //set
                    db.addDouble(updateKey, updateValue);
                } else {
                    //add
                    Double new_val = db.getDouble(updateKey);
                    new_val += updateValue;
                    db.addDouble(updateKey, new_val);
                }
            }
        }

        return check;
    }

    private boolean execOPE(String cond_key, Object tmp_cond_val, int cond, List<UpdateOP> list, String user_key, String sym_key) {
        Long val = db.getOPEInt(cond_key);
        Long cond_val = (Long) tmp_cond_val;

        int result = val.compareTo(cond_val);

        boolean check = checkResult(result, cond);

        if (check) {
            for (UpdateOP updateOp : list) {
                String updateKey = updateOp.getUpdateKey();
                Long updateValue = updateOp.getUpdateValueL();

                if (updateOp.getOp() == 0) {
                    //set
                    db.addOPEInt(updateKey, updateValue);
                } else {
                    //add
                    Long new_val = db.getOPEInt(updateKey);

                    try {
                        new_val = SconeClient.exec_ope_add(new_val, updateValue, user_key, sym_key);
                    } catch (ErrorExc errorExc) {
                        System.err.println("Could  not Exec OPE ADD");
                    }
                    db.addOPEInt(updateKey, new_val);
                }
            }
        }

        return check;
    }

    private boolean execSUM(String cond_key, Object tmp_cond_val, int cond, List<UpdateOP> list, String user_key, String sym_key) {
        BigInteger val = db.getSumInt(cond_key);
        BigInteger cond_val = (BigInteger) tmp_cond_val;

        int result = 0;
        try {
            result = SconeClient.exec_homo_relation(val, cond_val, user_key, sym_key);
        } catch (ErrorExc errorExc) {
            System.err.println("Could not Exec Homo Relation");
            return false;
        }

        boolean check = checkResult(result, cond);

        if (check) {
            for (UpdateOP updateOp : list) {
                String updateKey = updateOp.getUpdateKey();
                BigInteger updateValue = updateOp.getUpdateValueB();

                if (updateOp.getOp() == 0) {
                    //set
                    db.addSumInt(updateKey, updateValue);
                } else {
                    //add
                    BigInteger new_val = db.getSumInt(updateKey);
                    System.out.println("NEW: " + (new_val != null) + "UP: "+  (updateValue != null));
                    new_val = new_val.add(updateValue);
                    db.addSumInt(updateKey, new_val);
                }
            }
        }

        return check;
    }

    private boolean checkResult(int result, int cond) {
        if (result < 0)
            return cond == 1 || cond == 4;

        if (result == 0) {
            return cond == 0 || cond == 3 || cond == 5;
        }

        if (result > 0) {
            return cond == 1 || cond == 2;
        }

        return false;
    }
}
