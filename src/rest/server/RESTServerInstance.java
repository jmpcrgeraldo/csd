package rest.server;

import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import rest.RESTServiceAPI;
import tools.URLService;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RESTServerInstance {

    public static void main(String[] args) throws Exception {
        System.setProperty("javax.net.ssl.keyStore", "jks/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "changeme");

        Logger.getLogger(RESTServer.class.getName()).setLevel(Level.FINE);

        int id;

        if (args.length != 1) {
            System.err.println("Usage [ID]");
            return;
        }

        id = Integer.parseInt(args[0]);

        URI baseUri = UriBuilder.fromUri(RESTServiceAPI.SERVER_URI).port(URLService.getRestPort(id)).build();

        ResourceConfig config = new ResourceConfig();
        config.register(new RESTServer(id));

        JdkHttpServerFactory.createHttpServer(baseUri, config, SSLContext.getDefault());

        System.out.println("REST Server ready @ " + baseUri);
    }
}
