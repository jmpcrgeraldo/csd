package rest.request;

import tools.WrapperMap;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

public class RequestClass implements Serializable {

    private String reqString1;
    private String reqString2;
    private String reqString3;
    private double reqDouble1;
    private double reqDouble2;
    private long reqLong1;
    private long reqLong2;
    private BigInteger bigInt1;
    private BigInteger bigInt2;
    private BigInteger nSquare;
    private String signature;
    private String pubKey;
    private long nonce;
    private Object reqObject;
    private WrapperMap.TYPE reqType;
    private List<UpdateOP> reqList;

    /**
     * Empty constructor necessary for the correct functioning of Jersey REST.
     */
    public RequestClass() {

    }

    public Object getReqObject() {
        return reqObject;
    }

    public WrapperMap.TYPE getReqType() {
        return reqType;
    }

    public List<UpdateOP> getReqList() {
        return reqList;
    }

    public String getReqString1() {
        return reqString1;
    }

    public String getReqString2() {
        return reqString2;
    }

    public double getReqDouble1() {
        return reqDouble1;
    }

    public double getReqDouble2() {
        return reqDouble2;
    }

    public String getSignature() {
        return signature;
    }

    public long getNonce() {
        return nonce;
    }

    public long getReqLong1() {
        return this.reqLong1;
    }

    public long getReqLong2() {
        return this.reqLong2;
    }

    public String getPubKey() {
        return this.pubKey;
    }

    public BigInteger getBigInt2() {
        return bigInt2;
    }

    public String getReqString3() {
        return reqString3;
    }

    public RequestClass setReqString3(String reqString3) {
        this.reqString3 = reqString3;
        return this;
    }

    public RequestClass setReqList(List<UpdateOP> reqList) {
        this.reqList = reqList;
        return this;
    }

    public RequestClass setReqObject(Object reqObject) {
        this.reqObject = reqObject;
        return this;
    }

    public RequestClass setReqType(WrapperMap.TYPE reqType) {
        this.reqType = reqType;
        return this;
    }

    public RequestClass setBigInt2(BigInteger bigInt2) {
        this.bigInt2 = bigInt2;
        return this;
    }

    public BigInteger getNSquare() {
        return nSquare;
    }

    public RequestClass setNSquare(BigInteger nSquare) {
        this.nSquare = nSquare;
        return this;
    }

    public BigInteger getBigInt1() {
        return bigInt1;
    }

    public RequestClass setBigInt1(BigInteger bigInt1) {
        this.bigInt1 = bigInt1;
        return this;
    }

    public RequestClass setReqString1(String reqString1) {
        this.reqString1 = reqString1;
        return this;
    }

    public RequestClass setReqString2(String reqString2) {
        this.reqString2 = reqString2;
        return this;
    }

    public RequestClass setReqDouble1(double reqDouble1) {
        this.reqDouble1 = reqDouble1;
        return this;
    }

    public RequestClass setReqDouble2(double reqDouble2) {
        this.reqDouble2 = reqDouble2;
        return this;
    }

    public RequestClass setReqLong1(long reqLong1) {
        this.reqLong1 = reqLong1;
        return this;
    }

    public RequestClass setReqLong2(long reqLong2) {
        this.reqLong2 = reqLong2;
        return this;
    }

    public RequestClass setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public RequestClass setPubKey(String pubKey) {
        this.pubKey = pubKey;
        return this;
    }

    public RequestClass setNonce(long nonce) {
        this.nonce = nonce;
        return this;
    }
}
