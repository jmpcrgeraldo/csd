package rest.request;

import java.io.Serializable;
import java.math.BigInteger;

public class UpdateOP implements Serializable {

    private String updateKey;
    private int op;
    private BigInteger updateValueB;
    private Long updateValueL;
    private Double updateValueD;

    //Needed
    public UpdateOP() {

    }

    public UpdateOP(String updateKey, int op, BigInteger updateValueB) {
        this.updateKey = updateKey;
        this.op = op;
        this.updateValueB = updateValueB;
    }
    public UpdateOP(String updateKey, int op, Long updateValueL) {
        this.updateKey = updateKey;
        this.op = op;
        this.updateValueL = updateValueL;
    }
    public UpdateOP(String updateKey, int op, Double updateValueD) {
        this.updateKey = updateKey;
        this.op = op;
        this.updateValueD = updateValueD;
    }


    public String getUpdateKey() {
        return updateKey;
    }


    public int getOp() {
        return op;
    }

    public BigInteger getUpdateValueB() {
        return updateValueB;
    }

    public Long getUpdateValueL() {
        return updateValueL;
    }

    public Double getUpdateValueD() {
        return updateValueD;
    }
}
