package rest.client;

import bftsmart.reconfiguration.util.RSAKeyLoader;
import bftsmart.tom.util.TOMUtil;
import hlib.hj.mlib.HelpSerial;
import hlib.hj.mlib.HomoAdd;
import hlib.hj.mlib.HomoOpeInt;
import hlib.hj.mlib.PaillierKey;
import org.glassfish.jersey.client.ClientProperties;
import rest.RESTServiceAPI;
import rest.ServerRequestType;
import rest.reply.MessageInfo;
import rest.reply.ReplyClass;
import rest.request.RequestClass;
import rest.request.UpdateOP;
import tools.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.net.URI;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.*;
import java.util.logging.Logger;

public class ClientFunc {

    private static final Logger LOG = Logger.getLogger(ClientFunc.class.getName());
    private final int max_numb_faults;
    private final int max_retries;
    private WebTarget target;
    private Client client;
    private Signatures.KeyPairOut walletKeyPair;
    private Signatures.KeyPairOut opeKeyPair;
    private Signatures.KeyPairOut sumKeyPair;
    private String opeKey;
    private PaillierKey sumPKey;
    private List<String> walletKeys;
    private List<String> opeKeys;
    private List<String> sumKeys;
    private SecureRandom sr;


    public ClientFunc(int max_numb_faults, int max_retries, int timeout) {
        this.max_numb_faults = max_numb_faults;
        this.max_retries = max_retries;

        sr = new SecureRandom();

        walletKeys = new ArrayList<>(100);
        opeKeys = new ArrayList<>(100);
        sumKeys = new ArrayList<>(100);

        walletKeyPair = Signatures.generateKeys();
        opeKeyPair = Signatures.generateKeys();
        sumKeyPair = Signatures.generateKeys();
        sumPKey = HomoAdd.generateKey();

        // This is a attempt to generate a random key the works with HomoOpeInt without causing problems
        boolean cont = true;
        while (cont) {
            try {
                byte[] tmp = new byte[256];
                new Random().nextBytes(tmp);
                opeKey = Signatures.toBase64(tmp);

                HomoOpeInt o = new HomoOpeInt(opeKey);
                Random r = new Random();
                for (int i = 0; i < 1000; i++) {
                    int l = r.nextInt();
                    long p = o.encrypt(l);
                    if (o.decrypt(p) != l) {
                        throw new StackOverflowError();
                    }
                }
                cont = false;
            } catch (StackOverflowError e) {
                System.out.println("New Key");
                cont = true;
            }
        }

        URI startServer = URLService.getNextRestURL();

        client = ClientBuilder
                .newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .property(ClientProperties.CONNECT_TIMEOUT, timeout)
                .property(ClientProperties.READ_TIMEOUT, timeout)
                .build();


        target = client.target(startServer);
    }

    public String generateKeys() {
        walletKeys.add(Signatures.createRandomString(128));
        opeKeys.add(Signatures.createRandomString(128));
        sumKeys.add(Signatures.createRandomString(128));

        return "CREATED KEYS";
    }

    public String getSumKey(int id) {
        return sumKeys.get(id);
    }

    public String getOPEKey(int id) {
        return opeKeys.get(id);
    }

    public String[] getAllPubKeys() {
        List<String> helperList = new LinkedList<>();
        Iterator<String> walletKeysIterator = this.walletKeys.iterator();
        Iterator<String> opeKeysIterator = this.opeKeys.iterator();
        Iterator<String> sumKeysIterator = this.sumKeys.iterator();

        while (walletKeysIterator.hasNext()) {
            helperList.add("WALLET - " + walletKeysIterator.next() + "; OPE - " + opeKeysIterator.next() + "; SUM - " + sumKeysIterator.next());
        }

        return helperList.toArray(new String[0]);
    }


    private void changeServer() {
        URI nextServer = URLService.getNextRestURL();
        target = client.target(nextServer);
    }

    private long generateNonce() {
        return sr.nextLong();
    }

    public void makeCreateMoneyRequest(int keyID, double balance) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();
            String signature = Signatures.sign(idKey + balance + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqDouble1(balance).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.CREATE_MONEY_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .post(Entity.json(req));

                if (validateResponse(resp, nonce, ServerRequestType.CREATE_MONEY, idKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public void makeTransferMoneyRequest(int keyID, int destKeyId, double amount) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);
        String destKey = this.walletKeys.get(destKeyId);
        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + destKey + amount + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqString2(destKey).setReqDouble1(amount).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.TRANSFER_MONEY_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                if (validateResponse(resp, nonce, ServerRequestType.TRANSFER_MONEY, idKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public void makeTransferMoneyNoLimitRequest(int keyID, int destKeyId, double amount) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);
        String destKey = this.walletKeys.get(destKeyId);

        while (tries < max_retries) {
            long nonce = generateNonce();
            String signature = Signatures.sign(idKey + destKey + amount + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqString2(destKey).setReqDouble1(amount).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.TRANSFER_MONEY_NO_LIMIT_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                if (validateResponse(resp, nonce, ServerRequestType.TRANSFER_MONEY_NO_LIMIT, idKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public double makeCurrentAmountRequest(int keyID) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();
            String signature = Signatures.sign(idKey + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.CURRENT_AMOUNT_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                Verification<Double> vf = validateResponse(resp, nonce, ServerRequestType.CURRENT_AMOUNT, idKey);

                if (vf.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return vf.value;
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public List<String> makeCurrentAmountBetweenRequest(double lowerValue, double upperValue) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign("" + lowerValue + upperValue + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqDouble1(lowerValue).setReqDouble2(upperValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.CURRENT_AMOUNT_BETWEEN_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                Verification<List<String>> vf = validateResponse(resp, nonce, ServerRequestType.CURRENT_AMOUNT_BETWEEN, pubKey);

                if (vf.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return vf.value;
                    else
                        return new LinkedList<>();
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public Double makeAddDoubleRequest(int keyID, double value) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + value + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqDouble1(value).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.ADD_DOUBLE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<Double> v = validateResponse(resp, nonce, ServerRequestType.ADD_DOUBLE, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return v.value;
                    else
                        return -1D;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public Double makeSubDoubleRequest(int keyID, double value) {
        int tries = 0;

        String pubKey = this.walletKeyPair.pubKey;
        String prvKey = this.walletKeyPair.prvKey;
        String idKey = this.walletKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + value + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqDouble1(value).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.SUB_DOUBLE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<Double> v = validateResponse(resp, nonce, ServerRequestType.SUB_DOUBLE, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return v.value;
                    else
                        return -1D;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }


    public void makePutOPERequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.opeKeyPair.pubKey;
        String prvKey = this.opeKeyPair.prvKey;
        String idKey = this.opeKeys.get(keyID);
        HomoOpeInt homoOpeInt = new HomoOpeInt(opeKey);
        long encryptedValue = homoOpeInt.encrypt(unencryptedValue);

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + encryptedValue + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqLong1(encryptedValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.PUT_OPE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .post(Entity.json(req));

                if (validateResponse(resp, nonce, ServerRequestType.PUT_OPE, idKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public int makeGetOPERequest(int keyID) {
        int tries = 0;

        String pubKey = this.opeKeyPair.pubKey;
        String prvKey = this.opeKeyPair.prvKey;
        String idKey = this.opeKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();
            String signature = Signatures.sign(idKey + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.GET_OPE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                Verification<Long> vf = validateResponse(resp, nonce, ServerRequestType.GET_OPE, idKey);

                if (vf.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return decryptOPE(vf.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public List<String> makeGetOPEBetweenRequest(int unencryptedLowerValue, int unencryptedUpperValue) {
        int tries = 0;

        String pubKey = this.opeKeyPair.pubKey;
        String prvKey = this.opeKeyPair.prvKey;
        HomoOpeInt opeInt = new HomoOpeInt(opeKey);
        long lowerValue = opeInt.encrypt(unencryptedLowerValue);
        long upperValue = opeInt.encrypt(unencryptedUpperValue);

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(pubKey + lowerValue + upperValue + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(pubKey).setReqLong1(lowerValue).setReqLong2(upperValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.GET_OPE_BETWEEN_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                Verification<List<String>> vf = validateResponse(resp, nonce, ServerRequestType.GET_OPE_BETWEEN, pubKey);

                if (vf.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return vf.value;
                    else
                        return new LinkedList<>();
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public int makeAddOPERequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.opeKeyPair.pubKey;
        String prvKey = this.opeKeyPair.prvKey;
        String idKey = this.opeKeys.get(keyID);

        long encryptedValue = new HomoOpeInt(opeKey).encrypt(unencryptedValue);

        String sym_key = KeyHelper.loadSym();
        String enc_user_key = KeyHelper.encryptUserKey(opeKey, sym_key);
        String enc_sym_key = KeyHelper.encryptSymKey(sym_key, KeyHelper.loadPub());

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + encryptedValue + enc_user_key + enc_sym_key + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqString2(enc_user_key).setReqString3(enc_sym_key).setReqLong1(encryptedValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.ADD_OPE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<Long> v = validateResponse(resp, nonce, ServerRequestType.ADD_OPE, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return new HomoOpeInt(opeKey).decrypt(v.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public int makeSubOPERequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.opeKeyPair.pubKey;
        String prvKey = this.opeKeyPair.prvKey;
        String idKey = this.opeKeys.get(keyID);
        long encryptedValue = new HomoOpeInt(opeKey).encrypt(unencryptedValue);
        String sym_key = KeyHelper.loadSym();
        String enc_user_key = KeyHelper.encryptUserKey(opeKey, sym_key);
        String enc_sym_key = KeyHelper.encryptSymKey(sym_key, KeyHelper.loadPub());

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + encryptedValue + enc_user_key + enc_sym_key + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setReqString2(enc_user_key).setReqString3(enc_sym_key).setReqLong1(encryptedValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.SUB_OPE_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<Long> v = validateResponse(resp, nonce, ServerRequestType.SUB_OPE, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return new HomoOpeInt(prvKey).decrypt(v.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public void makePutSumIntRequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.sumKeyPair.pubKey;
        String prvKey = this.sumKeyPair.prvKey;
        String idKey = this.sumKeys.get(keyID);

        BigInteger encryptedValue = null;
        try {
            encryptedValue = HomoAdd.encrypt(new BigInteger(String.valueOf(unencryptedValue)), this.sumPKey);
        } catch (Exception e) {
            System.err.println("Could not Encrypt Value...");
            e.printStackTrace();
            return;
        }

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + encryptedValue + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setBigInt1(encryptedValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.PUT_SUM_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .post(Entity.json(req));

                if (validateResponse(resp, nonce, ServerRequestType.PUT_SUM_INT, idKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public int makeGetSumIntRequest(int keyID) {
        int tries = 0;

        String pubKey = this.sumKeyPair.pubKey;
        String prvKey = this.sumKeyPair.prvKey;
        String idKey = this.sumKeys.get(keyID);

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.GET_SUM_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<BigInteger> v = validateResponse(resp, nonce, ServerRequestType.GET_SUM_INT, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return decryptHomo(v.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public List<String> makeGetSumIntBetweenRequest(int unencryptedLowerValue, int unencryptedUpperValue) {
        int tries = 0;

        String pubKey = this.sumKeyPair.pubKey;
        String prvKey = this.sumKeyPair.prvKey;

        String sym_key = KeyHelper.loadSym();
        String enc_user_key = KeyHelper.encryptUserKey(HelpSerial.toString(this.sumPKey), sym_key);
        String enc_sym_key = KeyHelper.encryptSymKey(sym_key, KeyHelper.loadPub());

        BigInteger lowerValue = null;
        BigInteger upperValue = null;
        try {
            lowerValue = HomoAdd.encrypt(new BigInteger(String.valueOf(unencryptedLowerValue)), this.sumPKey);
            upperValue = HomoAdd.encrypt(new BigInteger(String.valueOf(unencryptedUpperValue)), this.sumPKey);
        } catch (Exception e) {
            System.err.println("Could not Encrypt Value...");
            e.printStackTrace();
            return new LinkedList<>();
        }

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(pubKey + lowerValue + upperValue + enc_user_key + enc_sym_key + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(pubKey).setReqString2(enc_user_key).setReqString3(enc_sym_key).setBigInt1(lowerValue).setBigInt2(upperValue).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.GET_SUM_BETWEEN_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(req, MediaType.APPLICATION_JSON));

                Verification<List<String>> vf = validateResponse(resp, nonce, ServerRequestType.GET_SUM_BETWEEN, pubKey);

                if (vf.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return vf.value;
                    else
                        return new LinkedList<>();
                }
            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }

        throw new RuntimeException("No servers available");
    }

    public int makeAddSumIntRequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.sumKeyPair.pubKey;
        String prvKey = this.sumKeyPair.prvKey;
        String idKey = this.sumKeys.get(keyID);
        BigInteger encryptedValue = null;
        try {
            encryptedValue = HomoAdd.encrypt(new BigInteger(String.valueOf(unencryptedValue)), this.sumPKey);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not Encrypt Value...");
            return -1;
        }

        while (tries < max_retries) {
            long nonce = generateNonce();

            String signature = Signatures.sign(idKey + encryptedValue + this.sumPKey.getNsquare() + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(idKey).setBigInt1(encryptedValue).setNSquare(this.sumPKey.getNsquare()).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.ADD_SUM_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<BigInteger> v = validateResponse(resp, nonce, ServerRequestType.ADD_SUM_INT, idKey);

                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return decryptHomo(v.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public int makeSubSumIntRequest(int keyID, int unencryptedValue) {
        int tries = 0;

        String pubKey = this.sumKeyPair.pubKey;
        String prvKey = this.sumKeyPair.prvKey;
        String idKey = this.sumKeys.get(keyID);
        long nonce = generateNonce();

        BigInteger encryptedValue = null;
        try {
            encryptedValue = HomoAdd.encrypt(new BigInteger(String.valueOf(unencryptedValue)), this.sumPKey);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        String signature = Signatures.sign(idKey + encryptedValue + this.sumPKey.getNsquare() + nonce, prvKey);

        RequestClass req = new RequestClass();
        req.setReqString1(idKey).setBigInt1(encryptedValue).setNSquare(this.sumPKey.getNsquare()).setNonce(nonce).setSignature(signature).setPubKey(pubKey);

        while (tries < max_retries) {
            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.SUB_SUM_PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                Verification<BigInteger> v = validateResponse(resp, nonce, ServerRequestType.SUB_SUM_INT, idKey);
                if (v.ok) {
                    if (resp.getStatusInfo().equals(Response.Status.OK))
                        return decryptHomo(v.value);
                    else
                        return -1;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    public void makeConditionalUpdateRequest(int condKeyId, int cond, int condVal, List<UpdateOP> updateOp, WrapperMap.TYPE type) {
        int tries = 0;

        String pubKey = null;
        String prvKey = null;
        String enc_user_key = null;
        String condKey = null;
        Object condValObj = null;
        String enc_sym_key = null;

        String sym_key = null;

        switch (type) {
            case DOUBLE:
                pubKey = this.walletKeyPair.pubKey;
                prvKey = this.walletKeyPair.prvKey;
                condKey = this.walletKeys.get(condKeyId);
                condValObj = condVal;
                break;
            case OPE_INT:
                pubKey = this.opeKeyPair.pubKey;
                prvKey = this.opeKeyPair.prvKey;

                sym_key = KeyHelper.loadSym();
                enc_user_key = KeyHelper.encryptUserKey(opeKey, sym_key);
                enc_sym_key = KeyHelper.encryptSymKey(sym_key, KeyHelper.loadPub());

                condKey = this.opeKeys.get(condKeyId);
                condValObj = new HomoOpeInt(opeKey).encrypt(condVal);
                break;
            case SUM_INT:
                pubKey = this.sumKeyPair.pubKey;
                prvKey = this.sumKeyPair.prvKey;

                sym_key = KeyHelper.loadSym();
                enc_user_key = KeyHelper.encryptUserKey(HelpSerial.toString(this.sumPKey), sym_key);
                enc_sym_key = KeyHelper.encryptSymKey(sym_key, KeyHelper.loadPub());

                condKey = this.sumKeys.get(condKeyId);
                try {
                    condValObj = HomoAdd.encrypt(new BigInteger(String.valueOf(condVal)), this.sumPKey);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

        while (tries < max_retries) {
            long nonce = generateNonce();
            String signature = Signatures.sign(condKey + condValObj + cond + updateOp + type + enc_user_key + enc_sym_key + nonce, prvKey);

            RequestClass req = new RequestClass();
            req.setReqString1(condKey).setReqObject(condValObj).setReqLong1(cond).setReqList(updateOp).setPubKey(pubKey)
                    .setReqType(type).setReqString2(enc_user_key).setSignature(signature).setReqString3(enc_sym_key).setNonce(nonce);

            try {
                Response resp = target
                        .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.COND_UPDATE)
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.json(req));

                if (validateResponse(resp, nonce, ServerRequestType.COND_UPDATE, condKey).ok) {
                    return;
                }

            } catch (ProcessingException e) {
                LOG.warning("Server Timeout: " + target.getUri());
                changeServer();
                tries++;
            }

            LOG.warning(":: " + target.getUri());
        }


        throw new RuntimeException("No servers available");
    }

    private int decryptOPE(Long opeV) {
        HomoOpeInt ope = new HomoOpeInt(opeKey);
        return ope.decrypt(opeV);
    }

    private int decryptHomo(BigInteger homoV) {
        try {
            return HomoAdd.decrypt(homoV, sumPKey).intValue();
        } catch (Exception e) {
            throw new RuntimeException("Invalid Homo");
        }
    }

    private <T> Verification<T> validateResponse(Response response, long expectedNonce, ServerRequestType expectedRequestType, String expectedUserKey) {
        MessageInfo[] messages = response.readEntity(MessageInfo[].class);

        int validated = 0;
        byte[] content = null;
        int null_msg = 0;

        if (!response.getStatusInfo().equals(Response.Status.OK)) {
            LOG.warning("STATUS:::: " + response.getStatusInfo());
        }

        if (messages == null) {
            LOG.warning("Messages are NULL");
            return new Verification<>(false, null);
        }

        for (MessageInfo msg : messages) {

            if (msg == null) {
                if (++null_msg > 1)
                    LOG.warning("NULL MESSAGE: " + null_msg);
                continue;
            }

            if (content == null) {
                content = msg.content;
            }

            if (!Arrays.equals(content, msg.content)) {
                LOG.warning("CONTENT NOT EQUAL");
                continue;
            }

            if (verifySignature(msg)) {
                LOG.info("VALID SIGNATURE");
                validated++;
            } else {
                LOG.warning("INVALID SIGNATURE");
            }
        }
        LOG.info("Validated " + validated + " of " + messages.length);
        if (validated >= max_numb_faults + 1) {
            ReplyClass rc = Conversion.getReplyObject(content);

            if (rc != null) {
                // Verify that the various ReplyClass fields are as expected
                if (!this.verifyReplyFields(rc, response.getStatus(), expectedNonce, expectedRequestType, expectedUserKey)) {
                    LOG.severe(buildVerificationMessage(rc));
                    return new Verification<>(false, null);
                }

                LOG.info(buildVerificationMessage(rc));
                return new Verification<>(true, rc.getValue() == null ? null : (T) rc.getValue());
            }
        }

        LOG.severe("NOT VALIDATED: Not f+1 replies: " + validated);
        return new Verification<>(false, null);
    }

    public boolean verifySignature(MessageInfo msg) {
        try {
            PublicKey pubKey = new RSAKeyLoader(msg.id, "config").loadPublicKey();
            boolean verified = TOMUtil.verifySignature(pubKey, msg.serializedMessage, msg.serializedMessageSignature);
            LOG.info("MESSAGE WAS " + (verified ? "" : "NOT") + " VERIFIED");
            return verified;
        } catch (Exception e) {
            LOG.severe("SIGNATURE NOT VERIFIED");
        }

        return false;
    }

    public boolean verifyReplyFields(ReplyClass reply, int expectedStatusCode, long expectedNonce, ServerRequestType expectedRequestType, String expectedUserKey) {
        if (reply.status != expectedStatusCode) {
            LOG.severe("Status Code Diff: " + reply.status + " != " + expectedStatusCode);
            return false;
        }
        if (reply.nonce != expectedNonce) {
            LOG.severe("Nonce Diff: " + reply.nonce + " != " + expectedNonce);
            return false;
        }
        if (!reply.request.equals(expectedRequestType)) {
            LOG.severe("Request Diff: " + reply.request + " != " + expectedRequestType);
            return false;
        }
        if (!reply.user_key.equals(expectedUserKey)) {
            LOG.severe("Key Diff: " + reply.user_key + " != " + expectedUserKey);
            return false;
        }

        return true;
    }

    class Verification<T> {
        public boolean ok;
        public T value;

        public Verification(boolean ok, T value) {
            this.ok = ok;
            this.value = value;
        }
    }

    public class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    private String buildVerificationMessage(ReplyClass rc) {
        HomoOpeInt homoOpeInt = new HomoOpeInt(this.opeKey);
        /*
        Integer opeInt = null;
        if (rc.opeValue != null)
            opeInt = homoOpeInt.decrypt(rc.opeValue);

        Integer sumInt = null;
        if (rc.sumInt != null) {
            try {
                sumInt = HomoAdd.decrypt(rc.sumInt, this.sumPKey).intValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        */


        String msg = "\n"
                + "\tStatus: " + rc.status + "\n"
                + "\tNonce: " + rc.nonce + "\n"
                + "\tRequest: " + rc.request + "\n"
                + "\tUser Key: " + rc.user_key + "\n"
                + "\tReply: " + rc.value + "\n"
                //+ "\tOPE: " + opeInt + "\n"
                + "\tOPE_List: " + rc.keyList + "\n";
                //+ "\tSumInt: " + sumInt;

        return msg;
    }
}
