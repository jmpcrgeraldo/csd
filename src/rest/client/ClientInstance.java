package rest.client;

import rest.request.UpdateOP;
import tools.Config;
import tools.WrapperMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class ClientInstance {

    private static final String START_STRING = "CLIENT STARTED";
    private static final String END_STRING = "CLIENT SHUTDOWN";

    private static final String COMMAND_QUIT = "Q";
    private static final String COMMAND_CREATE_MONEY = "CM";
    private static final String COMMAND_TRANSFER_MONEY = "TM";
    private static final String COMMAND_TRANSFER_MONEY_NO_LIMIT = "TMNL";
    private static final String CURRENT_AMOUNT = "CA";
    private static final String CURRENT_AMOUNT_BETWEEN = "CAB";
    private static final String SUB_DOUBLE = "SD";
    private static final String ADD_DOUBLE= "AD";
    private static final String GENERATE_PUB_KEY = "GK";
    private static final String GET_CURRENT_KEYS = "CK";
    private static final String PUT_OPE = "PO";
    private static final String GET_OPE = "GO";
    private static final String GET_OPE_BETWEEN = "GOB";
    private static final String ADD_OPE = "AO";
    private static final String SUB_OPE = "SO";
    private static final String PUT_SUM = "PS";
    private static final String GET_SUM = "GS";
    private static final String ADD_SUM = "AS";
    private static final String SUB_SUM = "SS";
    private static final String GET_SUM_BETWEEN = "GSB";
    private static final String COND_UP = "COND";
    private static final String HELP = "h";

    private static final Logger LOG = Logger.getLogger(ClientInstance.class.getName());

    private static ClientFunc clientF;

    public static void main(String[] args) throws IOException {
        System.setProperty("javax.net.ssl.trustStore", "jks/client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeme");

        int max_faults = Config.getProperty(Config.PROPS.client_max_numb_faults);
        int max_retires = Config.getProperty(Config.PROPS.client_max_retries);
        int timeout = Config.getProperty(Config.PROPS.client_timeout);

        clientF = new ClientFunc(max_faults, max_retires, timeout);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String command;

        LOG.info(START_STRING);

        while (true) {
            command = parseCommand(reader);

            switch (command) {
                case COMMAND_CREATE_MONEY:
                    parseCreateMoneyInfo(reader);
                    break;
                case COMMAND_TRANSFER_MONEY:
                    parseTransferMoneyInfo(reader);
                    break;
                case CURRENT_AMOUNT:
                    parseCurrentAmountInfo(reader);
                    break;
                case COMMAND_TRANSFER_MONEY_NO_LIMIT:
                    parseTransferMoneyNoLimitInfo(reader);
                    break;
                case PUT_OPE:
                    parsePutOPE(reader);
                    break;
                case GET_OPE:
                    parseGetOPE(reader);
                    break;
                case GET_OPE_BETWEEN:
                    parseGetOPEBetween(reader);
                    break;
                case PUT_SUM:
                    parsePutSum(reader);
                    break;
                case GET_SUM:
                    parseGetSum(reader);
                    break;
                case ADD_SUM:
                    parseAddSum(reader);
                    break;
                case SUB_SUM:
                    parseSubSum(reader);
                    break;
                case COND_UP:
                    parseConditionalUpdate(reader);
                    break;
                case GENERATE_PUB_KEY:
                    handleNewKeys();
                    continue;
                case GET_CURRENT_KEYS:
                    handlePrintKeys();
                    continue;
                case COMMAND_QUIT:
                    LOG.info(END_STRING);
                    return;
                case HELP:
                default:
                    System.err.println(COMMAND_QUIT + "\tQuit");
                    System.err.println(COMMAND_CREATE_MONEY + "\tCreate Money");
                    System.err.println(COMMAND_TRANSFER_MONEY + "\tTransfer Money");
                    System.err.println(COMMAND_TRANSFER_MONEY_NO_LIMIT + "\tTransfer Money No Limit");
                    System.err.println(CURRENT_AMOUNT + "\tCurrent Amount");
                    System.err.println(CURRENT_AMOUNT_BETWEEN + "\tCurrent Amount Between Values");
                    System.err.println(SUB_DOUBLE + "\tSubtract value from Double");
                    System.err.println(ADD_DOUBLE + "\tAdd value to Double");
                    System.err.println(GENERATE_PUB_KEY + "\tGenerate New Public Key");
                    System.err.println(GET_CURRENT_KEYS + "\tGet Current Keys");
                    System.err.println(PUT_OPE + "\tGet Put OPE");
                    System.err.println(GET_OPE + "\tGet OPE");
                    System.err.println(GET_OPE_BETWEEN + "\tGet OPE Between Values");
                    System.err.println(SUB_OPE + "\tSubtract value from OPE");
                    System.err.println(ADD_OPE + "\tAdd value to OPE");
                    System.err.println(PUT_SUM + "\tPut Sum Int");
                    System.err.println(GET_SUM + "\tGet Sum Int");
                    System.err.println(ADD_SUM + "\tAdd Value to Sum Int");
                    System.err.println(SUB_SUM + "\tSubtract value from Sum Int");
                    System.err.println(GET_OPE_BETWEEN + "\tGet SUM Between Values");
                    continue;
            }

        }
    }

    private static void parseConditionalUpdate(BufferedReader reader) throws IOException {
        int condKeyId = Integer.parseInt(reader.readLine());
        int condVal = Integer.parseInt(reader.readLine());
        int cond = Integer.parseInt(reader.readLine());

        int numConds = Integer.parseInt(reader.readLine());
        List<UpdateOP> updateOp = new LinkedList<>();
        for (int i = 0; i < numConds; i++) {
            String[] args = reader.readLine().split(" ");
            //UpdateOP op = new UpdateOP(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        }

        int typeId = Integer.parseInt(reader.readLine());
        WrapperMap.TYPE type = null;

        switch (typeId) {
            case 0:
                type = WrapperMap.TYPE.DOUBLE;
                break;
            case 1:
                type = WrapperMap.TYPE.OPE_INT;
                break;
            case 2:
                type = WrapperMap.TYPE.SUM_INT;
                break;
            default:
                System.err.println("ERRO A LER TIPO");
                return;
        }
        clientF.makeConditionalUpdateRequest(condKeyId, cond, condVal, updateOp, type);
    }

    private static void parseSubSum(BufferedReader reader) throws IOException {
        int keyId = Integer.parseInt(reader.readLine());
        int value = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Sub Sum");

        clientF.makeSubSumIntRequest(keyId, value);
    }

    private static void parseAddSum(BufferedReader reader) throws IOException {
        int keyId = Integer.parseInt(reader.readLine());
        int value = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Add Sum");

        clientF.makeAddSumIntRequest(keyId, value);
    }

    private static void parseGetSum(BufferedReader reader) throws IOException {
        int keyId = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Get Sum");

        clientF.makeGetSumIntRequest(keyId);
    }

    private static void parsePutSum(BufferedReader reader) throws IOException {
        int keyId = Integer.parseInt(reader.readLine());
        int value = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Put Sum");

        clientF.makePutSumIntRequest(keyId, value);
    }

    private static void parseGetOPEBetween(BufferedReader reader) throws IOException {
        int lower = Integer.parseInt(reader.readLine());
        int upper = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Get OPE BETWEEN");

        clientF.makeGetOPEBetweenRequest(lower, upper);
    }

    private static void parseGetOPE(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Get OPE");

        clientF.makeGetOPERequest(keyID);
    }

    private static void parsePutOPE(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());
        int value = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Put OPE");

        clientF.makePutOPERequest(keyID, value);

    }

    private static void parseCurrentAmountInfo(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());

        LOG.info("Requesting Current Amount");

        clientF.makeCurrentAmountRequest(keyID);

    }

    private static void parseTransferMoneyInfo(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());
        int destKeyID = Integer.parseInt(reader.readLine());
        double amount = Double.parseDouble(reader.readLine());

        LOG.info("Requesting Transfer Money");

        clientF.makeTransferMoneyRequest(keyID, destKeyID, amount);
    }

    private static void parseTransferMoneyNoLimitInfo(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());
        int destKeyID = Integer.parseInt(reader.readLine());
        double amount = Double.parseDouble(reader.readLine());

        LOG.info("Requesting Transfer Money No Limit");

        clientF.makeTransferMoneyNoLimitRequest(keyID, destKeyID, amount);
    }

    private static void parseCreateMoneyInfo(BufferedReader reader) throws IOException {
        int keyID = Integer.parseInt(reader.readLine());
        double amount = Double.parseDouble(reader.readLine());

        LOG.info("Requesting Create Money");

        clientF.makeCreateMoneyRequest(keyID, amount);
    }

    private static void handleNewKeys() {
        String pubKey = clientF.generateKeys();

        System.out.println(pubKey);
    }

    private static void handlePrintKeys() {
        String[] keys = clientF.getAllPubKeys();

        for (int i = 0; i < keys.length; i++) {
            System.out.printf("Key %d : %s\n", i, keys[i]);
        }
    }

    private static String parseCommand(BufferedReader reader) throws IOException {
        return reader.readLine().toUpperCase();
    }
}
