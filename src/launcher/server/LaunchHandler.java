package launcher.server;

import launcher.LaunchAPI;
import tools.KeyHelper;
import tools.Signatures;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static launcher.LaunchAPI.*;
import static tools.Signatures.toBase64;
import static tools.URLService.JAR_LOCATION;

@Path(LaunchAPI.RESOURCE_PATH)
public class LaunchHandler {
    private Map<Long, ThreadID> threads;
    private Set<Long > nonces;

    class ThreadID {
        Thread t;
        int id;

        public ThreadID(Thread t, int id) {
            this.t = t;
            this.id = id;
        }
    }

    LaunchHandler() {
        threads = new HashMap<>(10);
        nonces = new TreeSet<>();
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path(LAUNCH_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    public Response launch(@QueryParam(URL_PARAM) String url,
                           @QueryParam(SIGN_PARAM) String signature,
                           @QueryParam(CLASS_PARAM) String main_class,
                           @QueryParam(PARAM_PARAM) String parameters,
                           @QueryParam(NONCE) Long nonce) {

        System.err.println("Launching thread " + main_class);

        if(nonces.add(nonce)) {
            System.err.println("Duplicated Nonce: " + nonce);
            return Response.status(Response.Status.CONFLICT).build();
        }

        Method m;

        try {
            URL[] urls = new URL[1];
            File f = new File(JAR_LOCATION);

            try {
                String fileData = toBase64(Files.readAllBytes(f.toPath()));
                boolean result = Signatures.check_sign(signature, KeyHelper.loadPub(), fileData);

                if (!result) {
                    return Response.status(Response.Status.FORBIDDEN).entity("Invalid Signature").build();
                }

            } catch (IOException e) {
                System.err.println(e.getMessage());
                return Response.status(Response.Status.FORBIDDEN).entity("Invalid Signature").build();
            }

            urls[0] = f.toURI().toURL();
            URLClassLoader loader = new URLClassLoader(urls);

            Class c = loader.loadClass(main_class);


            m = c.getMethod("main", String[].class);
        } catch (MalformedURLException | ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
            return Response.status(Response.Status.FORBIDDEN).entity("Invalid Signature").build();
        }
        String[] params = parameters.split("/");
        Thread t = new Thread(() -> {
            try {
                m.invoke(null, (Object) params);
            } catch (IllegalAccessException | InvocationTargetException e) {
                System.err.println(e.getMessage());
            }
        });

        t.start();

        long thread_id = t.getId();

        int id = -1;
        try{
           id = Integer.parseInt(parameters);
        }catch (Exception ignore) {

        }

        threads.put(thread_id, new ThreadID(t, id));

        System.err.println("Launched Replica: " + id + " with Thread: " + thread_id);

        return Response.status(Response.Status.OK).entity(thread_id).build();
    }

    @GET
    @Path(STOP_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    public Response stop(@QueryParam(ID_PARAM) long id, @QueryParam(NONCE) Long nonce) {

        if(nonces.add(nonce)) {
            System.err.println("Duplicated Nonce: " + nonce);
            return Response.status(Response.Status.CONFLICT).build();
        }


        ThreadID tid = threads.remove(id);

        if(tid != null) {
            System.err.println("Stopping Replica: " + tid.id);
            tid.t.stop();//TODO does not actually stop...
            return Response.status(Response.Status.OK).build();
        }

        System.err.println("Thread not found" + id);
        return Response.status(Response.Status.NOT_FOUND).build();


    }
}
