package launcher.server;

import launcher.LaunchAPI;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.security.NoSuchAlgorithmException;

public class LaunchServer {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.setProperty("javax.net.ssl.keyStore", "jks/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "changeme");

        URI baseUri = UriBuilder.fromUri(LaunchAPI.SERVER_URI).port(LaunchAPI.SERVER_PORT).build();

        ResourceConfig config = new ResourceConfig();
        config.register(new LaunchHandler());

        JdkHttpServerFactory.createHttpServer(baseUri, config, SSLContext.getDefault());

        System.out.println("Replica Launcher ready @ " + baseUri);
    }

}