package launcher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(LaunchAPI.RESOURCE_PATH)
public interface LaunchAPI {
    String SERVER_URI = "https://127.0.0.1/";
    int SERVER_PORT = 9988;

    String RESOURCE_PATH = "/class";

    String LAUNCH_PATH = "/launch";
    String STOP_PATH = "/stop";

    String URL_PARAM = "url";
    String SIGN_PARAM = "sign";
    String CLASS_PARAM = "class";
    String PARAM_PARAM = "param";
    String NONCE = "nonce";

    String ID_PARAM = "id";

    @GET
    @Path(LAUNCH_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    Response launch(
            @QueryParam(URL_PARAM) String url,
            @QueryParam(SIGN_PARAM) String signature,
            @QueryParam(CLASS_PARAM) String main_class,
            @QueryParam(PARAM_PARAM) String parameters);

    @GET
    @Path(STOP_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    Response stop(@QueryParam(ID_PARAM) long id);

}
