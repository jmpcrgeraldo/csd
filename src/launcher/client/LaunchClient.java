package launcher.client;

import launcher.LaunchAPI;
import org.glassfish.jersey.client.ClientProperties;
import tools.KeyHelper;
import tools.Signatures;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;

import static tools.URLService.JAR_LOCATION;


public class LaunchClient {
    private static final String COMMAND_LAUNCH = "L";
    private static final String COMMAND_LAUNCH_ALL = "LA";
    private static final String COMMAND_STOP = "S";
    private static final String COMMAND_PING = "P";
    private static final String COMMAND_AUTO_START = "SA";
    private static final String COMMAND_AUTO_END = "EA";

    private static final String COMMAND_EXIT = "E";

    public static void main(String[] args) throws IOException {
        System.setProperty("javax.net.ssl.trustStore", "jks/client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeme");

        File f = new File(JAR_LOCATION);
        byte[] b = Files.readAllBytes(f.toPath());
        String default_sign = Signatures.sign(Signatures.toBase64(b), KeyHelper.loadPrv());
        System.out.println("JAR SIGN: " + default_sign);

        Client client = ClientBuilder
                .newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .property(ClientProperties.CONNECT_TIMEOUT, 1000)
                .property(ClientProperties.READ_TIMEOUT,    1000)
                .build();

        URI server = UriBuilder.fromUri(LaunchAPI.SERVER_URI).port(LaunchAPI.SERVER_PORT).build();
        ClientHandler c = new ClientHandler(client, server, default_sign);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String command;

        loop: while (true) {
            System.out.println("Enter command:");
            command = parseCommand(reader);

            switch (command) {
                case COMMAND_LAUNCH:
                    c.sendLaunch(reader);
                    break;
                case COMMAND_STOP:
                    c.sendStop(reader);
                    break;
                case COMMAND_PING:
                    c.sendPing(reader);
                    break;
                case COMMAND_AUTO_START:
                    c.startAutoMode();
                    break;
                case COMMAND_AUTO_END:
                    c.endAutoMode();
                    break;
                case COMMAND_LAUNCH_ALL:
                    c.sendLaunchAll(reader);
                    break;
                case COMMAND_EXIT:
                    break loop;
            }
        }
    }

    private static String parseCommand(BufferedReader reader) throws IOException {
        return reader.readLine().toUpperCase();
    }

    static class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}




