package launcher.client;

import launcher.LaunchAPI;
import org.glassfish.jersey.message.internal.Token;
import rest.RESTServiceAPI;
import tools.URLService;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static tools.URLService.JAR_LOCATION;

class ClientHandler {

    private WebTarget target;
    private Client client;

    private Thread t;
    private volatile boolean auto_run = false;

    private String sign_default;

    ClientHandler(Client client, URI server, String sign_default) {
        this.target = client.target(server);
        this.client = client;
        this.sign_default = sign_default;
    }

    public void sendLaunch(BufferedReader reader) throws IOException {
        System.out.print("Enter URL or EMPTY for DEFAULT: ");
        String url = reader.readLine();
        System.out.print("Enter SIGN or EMPTY for DEFAULT: ");
        String sign = reader.readLine();
        System.out.print("Enter CLASS MAIN or EMPTY for DEFAULT: ");
        String class_main = reader.readLine();
        System.out.print("Enter Param or EMPTY for DEFAULT: ");
        String param = reader.readLine();

        launch(url, sign, class_main, param);
    }


    public void sendLaunchAll(BufferedReader reader) throws IOException {
        System.out.print("Enter Count: ");
        int count = Integer.parseInt(reader.readLine());

        System.out.println("Sending Launch All");

        for (int i = 0; i < count; i++)
            launch("", "", "", String.valueOf(i));
    }

    private void launch(String url, String sign, String class_main, String param) {
        System.out.println("Sending launch " + param);

        try {
            Response resp = target
                    .path(LaunchAPI.RESOURCE_PATH + LaunchAPI.LAUNCH_PATH)
                    .queryParam(LaunchAPI.URL_PARAM, url.isEmpty() ? JAR_LOCATION : url)
                    .queryParam(LaunchAPI.SIGN_PARAM, sign.isEmpty() ? sign_default : sign)
                    .queryParam(LaunchAPI.CLASS_PARAM, class_main.isEmpty() ? "rest.server.RESTServerInstance" : class_main)
                    .queryParam(LaunchAPI.PARAM_PARAM, param.isEmpty() ? "0" : param)
                    .queryParam(LaunchAPI.NONCE, new Random().nextLong())
                    .request(MediaType.APPLICATION_JSON).get();

            System.out.println("ID: " + resp.readEntity(Long.class));
        } catch (ProcessingException e) {
            System.err.println("Server Timeout...");
        }
    }


    public void sendStop(BufferedReader reader) throws IOException {
        System.out.print("Enter ID: ");
        long id = Long.parseLong(reader.readLine());

        stop(id);

    }

    private void stop(long id) {
        System.out.println("Sending stop");
        try {
            Response resp = target
                    .path(LaunchAPI.RESOURCE_PATH + LaunchAPI.STOP_PATH)
                    .queryParam(LaunchAPI.ID_PARAM, id)
                    .queryParam(LaunchAPI.NONCE, new Random().nextLong())
                    .request(MediaType.APPLICATION_JSON).get();

            System.err.println("Status: " + resp.getStatus());
        } catch (ProcessingException e) {
            System.err.println("Server Timeout...");
        }
    }

    public boolean sendPing(BufferedReader reader) throws IOException {
        System.out.print("Enter ID: ");
        int id = Integer.parseInt(reader.readLine());

        return ping(id);

    }

    boolean ping(int id) {
        URI nextServer = URLService.getURL(id);
        WebTarget target = client.target(nextServer);

        System.out.println("Sending ping to " + nextServer.toString());
        try {
            Response resp = target
                    .path(RESTServiceAPI.SERVICE_PATH + RESTServiceAPI.PING_PATH)
                    .request(MediaType.APPLICATION_JSON).get();


            System.err.println("Status: " + resp.getStatus());

            return resp.getStatusInfo().equals(Response.Status.OK);
        } catch (ProcessingException e) {
            System.err.println("Server Timeout...");
            return false;
        }

    }

    public void startAutoMode() {
        System.out.println("Starting Auto Mode");
        auto_run = true;

        t = new Thread(
                () -> {
                    int id = 0;
                    boolean res;

                    while (auto_run) {
                        System.out.println("Running Check...");
                        res = ping(id);

                        if (!res) {
                            System.out.println("No Response from Replica " + id);
                            stop(id);
                            launch("", "", "", Integer.toString(id));
                            System.out.println("Replica " + id + " restarted");
                        }
                        System.out.println("Replica " + id + " is OK");
                        id = URLService.getNextURLSeq(id);

                        try {
                            TimeUnit.SECONDS.sleep(3);
                        } catch (InterruptedException e) {
                            System.err.println("Failed to sleep");
                        }
                    }
                }
        );

        t.start();
    }


    public void endAutoMode() {
        System.out.println("Ending Auto Mode");
        auto_run = false;

        t.interrupt();

    }

}