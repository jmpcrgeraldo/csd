package security;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * Esta classe engloba as opera��es de criptografia sim�trica
 */
final public class SymetricKey extends Key {
    static private SecureRandom rg = new SecureRandom();

    protected SymetricKey(String algorithm, java.security.Key key) {
        super(algorithm, key);
    }

    /**
     * Constr�i um chave sim�trica aleat�ria
     *
     * @throws Exception erro interno
     */
    public static SymetricKey createKey() throws Exception {
        return createKey(new String(rg.randomBytes(16)));
    }

    /**
     * Constr�i uma chave a partir de um segredo.
     *
     * @param secret segredo do qual derivar� a chave
     * @throws Exception erro interno
     */
    public static SymetricKey createKey(String secret) throws Exception {
        return createKey("AES", secret);
    }

    /**
     * Constr�i uma chave a partir de um segredo.
     *
     * @param algorithm Algoritmo a usar
     * @param secret    Segredo do qual derivar� a chave
     * @throws Exception erro interno
     */
    public static SymetricKey createKey(String algorithm, String secret) throws Exception {
        KeyGenerator kg = KeyGenerator.getInstance(algorithm);
        java.security.SecureRandom rg = java.security.SecureRandom.getInstance("sha1PRNG");
        rg.setSeed(secret.getBytes());
        kg.init(128, rg);
        return new SymetricKey(algorithm, kg.generateKey());
    }

    /**
     * Constr�i uma chave sim�trica a partir da sua representa��o externa
     *
     * @param data a representa��o externa da chave em bytes
     * @throws Exception - erro interno
     */
    public static SymetricKey createKey(byte[] key) throws Exception {
        return createKey("AES", key);
    }

    public static SymetricKey createKey(String algorithm, byte[] key) throws Exception {
        return new SymetricKey(algorithm, new SecretKeySpec(key, algorithm));
    }

}
