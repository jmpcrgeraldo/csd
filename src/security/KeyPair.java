package security;

import java.security.KeyPairGenerator;

/**
 * Esta classe permite gerar pares de chaves destinadas a opera��es de criptografia assim�trica.
 * Dimens�o m�xima das mensagens a cficrar: 117 bytes.
 */
final public class KeyPair {
    private PublicKey pubKey;
    private PrivateKey prvKey;

    protected KeyPair(PublicKey pub, PrivateKey priv) {
        this.pubKey = pub;
        this.prvKey = priv;

    }

    /**
     * Cria um par de chaves assim�tricas RSA.
     */
    public static KeyPair createKeyPair() {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            java.security.KeyPair kp = kpg.generateKeyPair();

            PublicKey pub = new PublicKey("RSA", kp.getPublic());
            PrivateKey priv = new PrivateKey("RSA", kp.getPrivate());

            return new KeyPair(pub, priv);
        } catch (Exception x) {
            x.printStackTrace();
        }
        return null;
    }

    /**
     * Devolve a chave p�blica do par.
     *
     * @return a chave p�blica que comp�e o par
     */
    public PublicKey getPublic() {
        return pubKey;
    }

    /**
     * Devolve a chave privada do par.
     *
     * @return a chave privada que comp�e o par
     */
    public PrivateKey getPrivate() {
        return prvKey;
    }

}
