package tests;

import rest.RESTServiceAPI;
import rest.client.ClientFunc;
import rest.request.UpdateOP;
import tools.URLService;
import tools.WrapperMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class OldPerformanceTests {
    private static final List<ClientFunc> clients = new LinkedList<>();
    private static final List<Thread> treads = new LinkedList<>();
    private static final List<List<Long>> res = new LinkedList<>();


    private static volatile AtomicInteger done = new AtomicInteger(0);
    private static int total = 0;

    private static int READ_BOUND;

    public static void main(String[] args) throws InterruptedException, IOException {
        //System.setProperty("javax.net.ssl.keyStore", "jks/server.jks");
        System.setProperty("javax.net.ssl.trustStore", "jks/client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeme");

        Logger.getLogger(ClientFunc.class.getName()).setLevel(Level.WARNING);

        Runtime.getRuntime().addShutdownHook(new Thread(
                OldPerformanceTests::flushServers
        ));

        //Disable all LOG prints
        //LogManager.getLogManager().reset();

        int clients = 1, tasks = 50, timeout = 2000, retries = 1;
        READ_BOUND = 90;

        if (args.length != 5) {
            System.out.println("Usage: clients actions");
        } else {
            clients = Integer.parseInt(args[0]);
            tasks = Integer.parseInt(args[1]);
            timeout = Integer.parseInt(args[2]);
            retries = Integer.parseInt(args[3]);
            READ_BOUND = Integer.parseInt(args[4]);
        }

        total = clients * tasks;

        run(clients, tasks, timeout, retries);
    }


    public static void run(int clients, int count, int timeout, int retries) throws InterruptedException, IOException {

        System.out.println("Running Clients");

        setupClients(clients, count, timeout, retries);

/*        Thread.UncaughtExceptionHandler h = (th, ex) -> {
            ex.printStackTrace();
            System.err.println("Client FAILED!!!");
            System.exit(-1);
        };

        for (Thread thread : treads) {
            thread.setUncaughtExceptionHandler(h);
            thread.start();
        }

        System.out.println("All Started");

        for (Thread tread : treads) {
            tread.join();
        }*/

        System.out.println("All Done");

        getResults(clients * count);

        flushServers();

        System.out.println("Completed");
    }

    private static void getResults(int ops) throws IOException {
        Path path = Paths.get("results.txt");

        List<Long> all_results = new LinkedList<>();
        res.forEach(all_results::addAll);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            for (final List<Long> row : res) {
                for (Long v : row) {
                    writer.write(String.format("%d\t", v));
                }
                writer.newLine();
            }

        }

        long total_time = 0;
        for (List<Long> re : res) {
            for (Long aLong : re) {
                total_time += aLong;
            }
        }

        Statistics st = new Statistics(all_results.stream().mapToDouble(Long::doubleValue).toArray());
        System.out.println();
        if (total_time == 0) return;
        if ((total_time / 1000) == 0)
            System.out.println("Ops/mil: " + (ops / (total_time)));
        else
            System.out.println("Ops/sec: " + (ops / (total_time / 1000)));
        System.out.printf("Mean:   %.2f\n" +
                "Var:    %.2f\n" +
                "Sd Dev: %.2f\n" +
                "Median: %.2f\n" +
                "Max:    %.2f\n" +
                "Min:    %.2f\n", st.getMean(), st.getVariance(), st.getStdDev(), st.getMedian(), st.getMax(), st.getMin());
    }


    private static void flushServers() {
        class InsecureHostnameVerifier implements HostnameVerifier {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        }
        ClientBuilder clientBuilder = ClientBuilder
                .newBuilder();
        clientBuilder.hostnameVerifier(new InsecureHostnameVerifier());
        Client client = clientBuilder
                .build();


        URLService.getAllServers().forEach(uri ->
                client.target(uri).path(RESTServiceAPI.SERVICE_PATH + "/flush").request().get()
        );
    }

    /*
        Method run by each Client
        Randomly selects a operation, key, client and amount
     */
    private static void startRequests(ClientFunc client, int actions_count) {
        client.generateKeys();
        client.makeCreateMoneyRequest(0, 100);

        int keyID = 0;
        int key;
        List<Long> avgs = new LinkedList<>();
        long time = 0;

        for (int i = 0; i < actions_count; i++) {
            switch (new Random().nextInt(2)) {
                case 0:
                    client.generateKeys();
                    //time = client.makeCreateMoneyRequest(++keyID, 100);
                    break;
                case 1:
                    // Limit amount of keys that each client creates
                    if (keyID < 20) {
                        key = new Random().nextInt(keyID + 1);
                       // time = client.makeCurrentAmountRequest(key);
                    }
                    break;
            }

            System.out.printf("Progress: %d / %d : %d\r", done.getAndIncrement(), total, time);

            if (time != 0) {
                avgs.add(time);
                time = 0;
            }

        }
        res.add(avgs);
    }

    private static void startRequests3(ClientFunc client, int actions_count) {
        Random r = new Random();

        int keyCount = 5;

        for (int i = 0; i < keyCount; i++) {
            client.generateKeys();
            int value_a = r.nextInt(10000);
            client.makePutSumIntRequest(i, value_a);
            int value_b = r.nextInt(10000);

            //client.makePutOPERequest(i, value_b);
        }


        int id;
        int value_a;
        int value_b;

        for (int i = 0; i < 1; i++) {
            id = r.nextInt(keyCount);
            value_a = r.nextInt(10000);
            value_b = r.nextInt(10000);

            //client.makePutSumIntRequest(id, value_a);

            //client.makePutOPERequest(id, value_b);

     /*       client.makeAddSumIntRequest(id, value_a);
            client.makeSubSumIntRequest(id, value_a);

            client.makeGetSumIntRequest(id);

            client.makeGetOPERequest(id);*/

            //System.out.printf("%s\r", client.makeGetOPEBetweenRequest(0, 10000).toString());

            System.out.printf("Progress: %d / %d\r", done.getAndIncrement(), total);
            client.makePutSumIntRequest(id, value_a);
            client.makeConditionalUpdateRequest(id, 0, value_a, getRandomList(client.getSumKey(0), BigInteger.valueOf(999)), WrapperMap.TYPE.SUM_INT);
        }
    }


    private static void startRequests2(ClientFunc client, int actions_count) {
        Random r = new Random();
        Map<Integer, Integer> ope = new HashMap<>(100);
        Map<Integer, Integer> homo = new HashMap<>(100);

        int keyCount = new Random().nextInt(20);

        IntStream.rangeClosed(0, keyCount).parallel().forEach(
                i -> {
                    client.generateKeys();
                }
        );

        int id;
        int value_a;
        int value_b;


        for (int i = 0; i < actions_count; i++) {
            id = new Random().nextInt(keyCount);
            value_a = new Random().nextInt(10000);
            value_b = new Random().nextInt(10000);
            switch (r.nextInt(3)) {
                case 0:

                    switch (getReadWrite()) {
                        case 0:
                            client.makeGetSumIntRequest(id);
                            //if(homo.get(id) != client.makeGetSumIntRequest(id)) System.err.println("----ERROR HOMO: "  + homo.get(id) + "!=" + client.makeGetSumIntRequest(id));
                            break;
                        case 1:
                            switch (r.nextInt(3)) {
                                case 0:
                                    homo.put(id, value_a);
                                    client.makePutSumIntRequest(id, value_a);
                                    break;
                                case 1:
                                    if (homo.get(id) != null) homo.put(id, homo.get(id) + value_a);
                                    client.makeAddSumIntRequest(id, value_a);
                                    break;
                                case 2:
                                    if (homo.get(id) != null) homo.put(id, homo.get(id) - value_a);
                                    client.makeSubSumIntRequest(id, value_a);
                                    break;
                            }
                            break;
                    }
                    break;
                case 1:
                    switch (getReadWrite()) {
                        case 0:
                            client.makeGetOPERequest(id);
                            // if(ope.get(id) != client.makeGetOPERequest(id)) System.err.println("---ERROR OPE: " + ope.get(id) +"!="+ client.makeGetOPERequest(id));
                            break;
                        case 1:
                            switch (r.nextInt(2)) {
                                case 0:
                                    ope.put(id, value_a);
                                    client.makePutOPERequest(id, value_a);
                                    break;
                                case 1:
                                    List<String> s = client.makeGetOPEBetweenRequest(value_a, value_b);
                                    //TODO
                                    break;
                            }
                    }
                    break;
                case 2:
                    //client.makeConditionalUpdateRequest(id, getRandomCond(), value_a, getRandomList(), getRandomType());
                    break;
            }
        }
    }

    private static int getRandomCond() {
        // Values between 0-5
        return new Random().nextInt(6);
    }

    private static List<UpdateOP> getRandomList(String updateKey, BigInteger updateValue) {
        int size = new Random().nextInt(10);

        List<UpdateOP> list = new LinkedList<>();

        for (int i = 0; i < size; i++) {

            UpdateOP op = new UpdateOP(updateKey, 1, updateValue);

            list.add(op);
        }

        return list;
    }

    private static WrapperMap.TYPE getRandomType() {

        WrapperMap.TYPE[] tmp = WrapperMap.TYPE.values();

        int rd = new Random().nextInt(tmp.length);

        return tmp[rd];
    }


    /*
     Return a value in [0,1] ;; 0->read ;; 1->write
  */
    private static int getReadWrite() {
        int a = new Random(System.currentTimeMillis()).nextInt(101);

        if (0 < a && a < READ_BOUND)
            return 0;

        return 1;
    }


    /*
        Return a value in [0,1,2]
     */
    @Deprecated
    private static int getAction() {

        long a = new Random(System.currentTimeMillis()).nextInt(101);

        //Current Amount
        if (0 <= a && a < 90)
            return 1;

        //Create Money
        if (90 <= a && a < 95)
            return 0;

        //Transfer
        return 2;

        //return new SecureRandom().nextInt(3);
    }

    /*
        Creates a thread for each Client
     */
    private static void setupClients(int count, int actions, int timeout, int retries) {

        //for (int id = 0; id < count; id++) {
        ClientFunc client = new ClientFunc(1, retries, timeout);
        //clients.add(client);
        //treads.add(new Thread(() -> startRequests3(client, actions)));
        startRequests(client, actions);
    }
}
