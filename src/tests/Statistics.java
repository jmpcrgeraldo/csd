package tests;

import java.util.Arrays;

class Statistics {
    private final double[] data;
    private final int size;

    Statistics(double[] data) {
        this.data = data;
        size = data.length;
    }

    double getMean() {
        double sum = 0.0;
        for (double a : data)
            sum += a;
        return sum / size;
    }

    double getVariance() {
        double mean = getMean();

        double sqDiff = 0.0;
        for (int i = 0; i < size; i++)
            sqDiff += (data[i] - mean) * (data[i] - mean);

        return sqDiff / size;
    }

    double getStdDev() {
        return Math.sqrt(getVariance());
    }


    double getMax() {
        double max = data[0];

        for (int i = 1; i < size; i++)
            if (data[i] > max)
                max = data[i];

        return max;
    }

    double getMin() {
        double min = data[0];

        for (int i = 1; i < size; i++)
            if (data[i] < min)
                min = data[i];

        return min;
    }

    double getMedian() {
        Arrays.sort(data);
        if (data.length % 2 == 0)
            return (data[(data.length / 2) - 1] + data[data.length / 2]) / 2.0;
        return data[data.length / 2];
    }
}