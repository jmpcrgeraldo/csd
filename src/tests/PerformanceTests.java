package tests;

import hlib.hj.mlib.HelpSerial;
import hlib.hj.mlib.HomoAdd;
import hlib.hj.mlib.HomoOpeInt;
import hlib.hj.mlib.PaillierKey;
import rest.RESTServiceAPI;
import rest.client.ClientFunc;
import tools.KeyHelper;
import tools.Signatures;
import tools.URLService;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PerformanceTests {
    static PaillierKey pk = HomoAdd.generateKey();
    static HomoOpeInt ope;
    static String opeKey;

    static String sym_key = KeyHelper.loadSym();
    static String pub_key = KeyHelper.loadPub();
    static String prv_key = KeyHelper.loadPrv();
    static Random rd = new Random();

    static ArrayList<Object> list = new ArrayList<>(1000000000);

    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.trustStore", "jks/client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeme");

        Logger.getLogger(ClientFunc.class.getName()).setLevel(Level.WARNING);

        byte[] tmp = new byte[256];
        new Random().nextBytes(tmp);
        opeKey = Signatures.toBase64(tmp);
        ope = new HomoOpeInt(opeKey);

        for (int i = 0; i < 10000; i++) {
            long b = ope.encrypt(i);
            ope.decrypt(b);
            new Random().nextInt(10);
        }
        System.out.println();

        long start = System.currentTimeMillis();
        /*for(int i = 0; i < 100; i++) {
            simulateEncDec(false);
        }
        System.out.println("TIME 1: " + (System.currentTimeMillis() - start));*/

        int count = 10000;
        long opee = 0, opes = 0, opec = 0;
        //doubleE(count);

        for(int i = 0; i < 20; i++) {
            opes += opeSum(count);
            opec += opeCmp(count);
            opee += opeE(count);

            // addE(count);

            // doubleSum(count);

            // addSum(count);

            // doubleCmp(count);


            // addCmp(count);

        }

        System.out.println("E: " + opee / 20);
        System.out.println("S: " + opes / 20);
        System.out.println("C: " + opec / 20);

        System.exit(-1);

        start = System.currentTimeMillis();
        ClientFunc client = new ClientFunc(1, 5, 10000);
        Random r = new Random();

        flushServers();

        for (int i = 0; i < count; i++) {
            client.generateKeys();
            client.makeCreateMoneyRequest(i, r.nextInt(1000) + 1);
            client.makePutOPERequest(i, r.nextInt(100) + 1);
            client.makePutSumIntRequest(i, r.nextInt(100) + 1);
        }

        System.out.println("Starting");
        start = System.currentTimeMillis();
        runNonEncrypted(client, count, false);

        System.out.println("TIME 1: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        runEncrypted(client, count, false);

        System.out.println("TIME 2: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        runHomoOPE(client, count, false);

        System.out.println("TIME 3: " + (System.currentTimeMillis() - start));

        System.out.println("-------------------------------------------------------------");
        start = System.currentTimeMillis();
        runNonEncrypted(client, count, true);

        System.out.println("TIME 4: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        runEncrypted(client, count, true);

        System.out.println("TIME 5: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        runHomoOPE(client, count, true);

        System.out.println("TIME 6: " + (System.currentTimeMillis() - start));
    }

    static long doubleE(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            double a = (double) r();
            double b = (double) (r());
            list.add(a);
        }

        return (System.currentTimeMillis() - start);
    }

    static long opeE(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            long a = ope.encrypt(r());
            long b = ope.encrypt(r());
            list.add(a);
        }

        return (System.currentTimeMillis() - start);
    }

    static long addE(int count) {
        list.clear();
        long start = System.currentTimeMillis();


        for (int i = 0; i < count; i++) {
            try {
                BigInteger a = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                BigInteger b = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                list.add(a);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (System.currentTimeMillis() - start);
    }

    static long doubleSum(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            double a = (double) r();
            double b = (double) (r());
            double c = a + b;
            list.add(c);
        }

        return (System.currentTimeMillis() - start);
    }

    static int r() {
        return rd.nextInt(1000000) + 1;
    }

    static long opeSum(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            long a = ope.encrypt(r());
            long b = ope.encrypt(r());
            //simulateEncDec(true);
            int c = ope.decrypt(a) + ope.decrypt(b);
            long d = ope.encrypt(c);
            list.add(d);
        }

        return (System.currentTimeMillis() - start);
    }

    static long addSum(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            try {
                BigInteger a = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                BigInteger b = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                BigInteger c = HomoAdd.sum(a, b, pk.getNsquare());
                int d = HomoAdd.decrypt(c, pk).intValue();
                list.add(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (System.currentTimeMillis() - start);
    }

    static long doubleCmp(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            double a = (double) r();
            double b = (double) (r());
            boolean c;
            if (i % 2 == 0)
                c = a > b;
            else c = b > a;
            list.add(c);
        }

        return (System.currentTimeMillis() - start);
    }

    static long opeCmp(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            long a = ope.encrypt(r());
            long b = ope.encrypt(r());
            boolean c;
            if (i % 2 == 0)
                c = a > b;
            else c = b > a;
            list.add(c);
        }

        return (System.currentTimeMillis() - start);
    }

    static long addCmp(int count) {
        list.clear();
        long start = System.currentTimeMillis();

        for (int i = 0; i < count; i++) {
            try {
                BigInteger a = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                BigInteger b = HomoAdd.encrypt(BigInteger.valueOf(r()), pk);
                //simulateEncDec(false);
                int c = HomoAdd.decrypt(a, pk).intValue();
                int d = HomoAdd.decrypt(b, pk).intValue();
                boolean e;
                if (i % 2 == 0)
                    e = d > c;
                else e = d > c;
                list.add(e);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (System.currentTimeMillis() - start);
    }

    static void simulateEncDec(boolean is_ope) {

        String enc_user_key;
        if (is_ope)
            enc_user_key = KeyHelper.encryptUserKey(opeKey, sym_key);
        else
            enc_user_key = KeyHelper.encryptUserKey(HelpSerial.toString(pk), sym_key);

        String enc_sym_key = KeyHelper.encryptSymKey(sym_key, pub_key);

        String sym_key_dec = KeyHelper.decryptSymKey(enc_sym_key, prv_key);
        String user_key_dec = KeyHelper.decryptUserKey(enc_user_key, sym_key_dec);

        PaillierKey pk;
        if (!is_ope) {
            pk = (PaillierKey) HelpSerial.fromString(user_key_dec);
            list.add(pk);
        } else list.add(user_key_dec);


    }


    static void runNonEncrypted(ClientFunc client, int count, boolean between) {
        Random r = new Random();

        for (int i = 0; i < count; i++) {

            if (between)
                client.makeCurrentAmountBetweenRequest((double) r.nextInt(1000) + 1, (double) r.nextInt(1000) + 1);
            else
                client.makeAddDoubleRequest(i, r.nextInt(1000) + 1);
        }
    }


    static void runEncrypted(ClientFunc client, int count, boolean between) {
        Random r = new Random();

        for (int i = 0; i < count; i++) {
            if (between)
                client.makeGetOPEBetweenRequest(r.nextInt(), r.nextInt(100) + 1);
            else
                client.makeAddSumIntRequest(i, r.nextInt(1000) + 1);
        }
    }

    static void runHomoOPE(ClientFunc client, int count, boolean between) {
        Random r = new Random();

        for (int i = 0; i < count; i++) {
            if (between)
                client.makeGetSumIntBetweenRequest(r.nextInt(100) + 1, r.nextInt(100 + 1));
            else
                client.makeSubOPERequest(i, r.nextInt(100) + 1);
        }
    }


    private static void flushServers() {
        class InsecureHostnameVerifier implements HostnameVerifier {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        }
        ClientBuilder clientBuilder = ClientBuilder
                .newBuilder();
        clientBuilder.hostnameVerifier(new InsecureHostnameVerifier());
        Client client = clientBuilder
                .build();


        URLService.getAllServers().forEach(uri ->
                client.target(uri).path(RESTServiceAPI.SERVICE_PATH + "/flush").request().get()
        );
    }

}