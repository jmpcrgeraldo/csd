package hlib;

import java.math.BigInteger;

import hlib.hj.mlib.HelpSerial;
import hlib.hj.mlib.HomoAdd;
import hlib.hj.mlib.PaillierKey;

/**
 * Testa utilizacao de cifra que permite adicionar valores com dados cifrados.
 */
public class TestSum
{

	public static void myMain() {
		int a = 1024;
		int b = 1000;

		PaillierKey pk = HomoAdd.generateKey();

		try {
			BigInteger A = HomoAdd.encrypt(new BigInteger(String.valueOf(a)), pk);
			BigInteger B = HomoAdd.encrypt(new BigInteger(String.valueOf(b)), pk);

			BigInteger R = HomoAdd.dif(A, B, pk.getNsquare());

			int r = HomoAdd.decrypt(R, pk).intValue();

			System.out.println("Result = " + r);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		myMain();
		return;

		/*try {
			PaillierKey pk = HomoAdd.generateKey();
			pk.printValues();
			BigInteger big1 = new BigInteger("33");
			BigInteger big2 = new BigInteger("22");
			BigInteger big1Code = HomoAdd.encrypt(big1, pk);				
			BigInteger big2Code = HomoAdd.encrypt(big2, pk);
			System.out.println("big1:     " + big1);
			System.out.println("big2:     " + big2);
			System.out.println("big1Code: " + big1Code);
			System.out.println("big2Code: " + big2Code);
			BigInteger big1plus2Code = HomoAdd.sum(big1Code, big2Code, pk.getNsquare());
			System.out.println("big1+big2 Code: " + big1plus2Code);
			BigInteger big1plus2 = HomoAdd.decrypt(big1plus2Code, pk);
			System.out.println("Resultado = " + big1plus2.intValue());

			System.out.println("Teste de subtracao");
			BigInteger big1minus2Code = HomoAdd.dif(big1Code, big2Code, pk.getNsquare());
			System.out.println("big1-big2 Code: " + big1minus2Code);
			BigInteger big1minus2 = HomoAdd.decrypt(big1minus2Code, pk);
			System.out.println("Resultado = " + big1minus2.intValue());

			// Test key serialization
			String chaveGuardada = "";

			chaveGuardada = HelpSerial.toString(pk);

			System.out.println("Chave guardada: " + chaveGuardada);
			// Test with saved key
			PaillierKey pk2 = null;
			BigInteger op3 = null;
			pk2 = (PaillierKey) HelpSerial.fromString(chaveGuardada);
			op3 = HomoAdd.decrypt(big1minus2, pk2);
			System.out.println("Subtracao: " + op3);
		} catch (Exception e) {
			e.printStackTrace();
		}*/

	}

}
