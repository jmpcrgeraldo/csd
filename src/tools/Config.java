package tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {

    private static final Map<PROPS, Integer> configs = new HashMap<>();
    private static final String configLocation = "config/config.properties";

    static {
        loadConfig();
    }

    public static int getProperty(PROPS prop) {
        return configs.get(prop);
    }

    private static void loadConfig() {
        try (InputStream inputStream = new FileInputStream(configLocation)) {

            Properties properties = new Properties();
            properties.load(inputStream);

            for (PROPS p : PROPS.values()) {
                configs.put(p, Integer.parseInt(properties.getProperty(p.toString())));
            }

        } catch (IOException e) {
            System.err.println("Error loading config file");
            System.exit(-1);
        }
    }


    public enum PROPS {
        client_timeout,
        client_max_retries,
        client_max_numb_faults,
        server_byzantine
    }


}
