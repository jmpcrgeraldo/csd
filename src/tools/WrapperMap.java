package tools;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WrapperMap implements Serializable {

    public enum TYPE {
        DOUBLE, OPE_INT, SUM_INT
    }

    private ConcurrentMap<String, Double> doubleMap;
    private ConcurrentMap<String, Long> opeIntMap;
    private ConcurrentMap<String, BigInteger> sumIntMap;

    private static final int CAPACITY = 1000;

    public WrapperMap() {
        this.doubleMap = new ConcurrentHashMap<>(CAPACITY);
        this.opeIntMap = new ConcurrentHashMap<>(CAPACITY);
        this.sumIntMap = new ConcurrentHashMap<>(CAPACITY);
    }

    public void addDouble(String key, Double val) {
        this.doubleMap.put(key, val);
    }

    public Double getDouble(String key) {
        return this.doubleMap.get(key);
    }

    public void removeDouble(String key) {
        this.doubleMap.remove(key);
    }

    public void addOPEInt(String key, Long val) {
        this.opeIntMap.put(key, val);
    }

    public Long getOPEInt(String key) {
        return this.opeIntMap.get(key);
    }

    public Collection<Map.Entry<String, Long>> getOPEEntries() {
        return this.opeIntMap.entrySet();
    }

    public Collection<Map.Entry<String, Double>> getDoubleEntries() {
        return this.doubleMap.entrySet();
    }

    public Collection<Map.Entry<String, BigInteger>> getSumEntries() {
        return this.sumIntMap.entrySet();
    }

    public void addSumInt(String key, BigInteger val) {
        this.sumIntMap.put(key, val);
    }

    public BigInteger getSumInt(String key) {
        return this.sumIntMap.get(key);
    }

    public void removeSumInt(String key) {
        this.sumIntMap.remove(key);
    }

    public void clear() {
        this.doubleMap = new ConcurrentHashMap<>(CAPACITY);
        this.opeIntMap = new ConcurrentHashMap<>(CAPACITY);
        this.sumIntMap = new ConcurrentHashMap<>(CAPACITY);
    }
}
