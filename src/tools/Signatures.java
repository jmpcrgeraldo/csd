package tools;

import security.*;

import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

public class Signatures {

    public static String sign(String value, String private_key) {
        try {
            String secret = digest(value);
            byte[] secretByes = toBytes(secret);

            byte[] prvKeyArr = toBytes(private_key);
            PrivateKey prv = PrivateKey.createKey(prvKeyArr);

            byte[] cipherText = prv.encrypt(secretByes);

            return toBase64(cipherText);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error signing");
        }
    }

    public static boolean check_sign(String signature, String public_key, String value) {

        try {
            byte[] pubKeyArr = toBytes(public_key);

            PublicKey pub = PublicKey.createKey(pubKeyArr);

            byte[] singBytes = pub.decrypt(toBytes(signature));

            byte[] valBytes = toBytes(digest(value));

            return Arrays.equals(singBytes, valBytes);

        } catch (Exception e) {
            System.err.println("Error verifying signature: " + e.getMessage());
            return false;
        }
    }

    public static KeyPairOut generateKeys() {
        KeyPair pair = KeyPair.createKeyPair();

        Objects.requireNonNull(pair);

        PublicKey pub = pair.getPublic();
        PrivateKey prv = pair.getPrivate();

        byte[] pubKeyArr = pub.exportKey();
        byte[] prvKeyArr = prv.exportKey();

        String pubKey = toBase64(pubKeyArr);
        String prvKey = toBase64(prvKeyArr);

        return new KeyPairOut(pubKey, prvKey);
    }

    private static String digest(String clearText) throws Exception {

        byte[] msgArr = clearText.getBytes();

        byte[] d2Arr = Digest.getDigest(msgArr);

        return toBase64(d2Arr);
    }

    public static String toBase64(byte[] arr) {
        return Base64.getEncoder().encodeToString(arr);
    }

    public static byte[] toBytes(String val) {
        return Base64.getDecoder().decode(val);
    }

    public static class KeyPairOut {
        public String pubKey, prvKey;

        KeyPairOut(String pubKey, String prvKey) {
            this.pubKey = pubKey;
            this.prvKey = prvKey;
        }
    }

    public static String createRandomString(int size) {
        return Base64.getEncoder().encodeToString(new SecureRandom().randomBytes(size));
    }
}
