package tools;

import security.PrivateKey;
import security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

public class KeyHelper {

    private static final String PRIVATE_KEY = System.getProperty("user.dir") + "/keys/private.der";
    private static final String PUBLIC_KEY = System.getProperty("user.dir") + "/keys/public.der";
    private static final String SYM_KEY = System.getProperty("user.dir") + "/keys/sym.jck";

    private static final char[] KEYSTORE_PASS = "changeit".toCharArray();
    private static final String KEYSTORE_TYPE = "JCEKS";
    private static final String KEYSTORE_ALIAS = "sym";

    private static final String ASYM_KEY_ALGO = "RSA";
    private static final String KEY_ALG = "AES";
    private static final String CIPHER_ALG = "AES/ECB/PKCS5Padding";


    public static String loadSym() {
        try {
            FileInputStream is = new FileInputStream(SYM_KEY);
            KeyStore keystore = KeyStore.getInstance(KEYSTORE_TYPE);
            keystore.load(is, KEYSTORE_PASS);
            Key key = keystore.getKey(KEYSTORE_ALIAS, KEYSTORE_PASS);

            return Signatures.toBase64(key.getEncoded());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String loadPrv() {
        try {
            byte[] keyBytes = loadFile(PRIVATE_KEY);

            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance(ASYM_KEY_ALGO);
            RSAPrivateKey prvKey = (RSAPrivateKey) kf.generatePrivate(spec);

            return Signatures.toBase64(prvKey.getEncoded());

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String loadPub() {
        try {
            byte[] keyBytes = loadFile(PUBLIC_KEY);

            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance(ASYM_KEY_ALGO);
            RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(spec);

            return Signatures.toBase64(pubKey.getEncoded());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] loadFile(String filename) {
        File file = new File(filename);

        try (FileInputStream fis = new FileInputStream(file);
             DataInputStream dis = new DataInputStream(fis)) {

            byte[] bytes = new byte[(int) file.length()];
            dis.readFully(bytes);

            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[]{};
        }
    }

    public static String encryptUserKey(String value, String key) {
        return runCipher(value, key, Cipher.ENCRYPT_MODE);
    }

    public static String decryptUserKey(String value, String key) {
        return runCipher(value, key, Cipher.DECRYPT_MODE);
    }

    private static String runCipher(String value, String key, int mode) {
        Objects.requireNonNull(value, "Value is NULL");
        Objects.requireNonNull(key, "Key is NULL");


        try {
            byte[] valueBytes = Signatures.toBytes(value);
            byte[] keyBytes = Signatures.toBytes(key);

            SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, KEY_ALG);

            Cipher cipherAes = Cipher.getInstance(CIPHER_ALG);
            cipherAes.init(mode, secretKeySpec);
            byte[] encryptedBytes = cipherAes.doFinal(valueBytes);

            return Signatures.toBase64(encryptedBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String encryptSymKey(String value, String public_key) {
        try {
            byte[] secretByes = Signatures.toBytes(value);
            byte[] pubKeyArr = Signatures.toBytes(public_key);
            PublicKey pub = PublicKey.createKey(pubKeyArr);

            byte[] cipherText = pub.encrypt(secretByes);

            return Signatures.toBase64(cipherText);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error encrypting");
        }
    }

    public static String decryptSymKey(String value, String private_key) {

        try {
            byte[] prvKeyArr = Signatures.toBytes(private_key);
            PrivateKey prv = PrivateKey.createKey(prvKeyArr);

            byte[] enc = prv.decrypt(Signatures.toBytes(value));

            return Signatures.toBase64(enc);

        } catch (Exception e) {
            System.err.println("Error decrypting: " + e.getMessage());
            return null;
        }
    }
}
