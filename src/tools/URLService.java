package tools;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;


public class URLService {

    private static final String CONFIG_HOME = "config/hosts.config";

    private static final Map<Integer, URLService.ServerData> urls = new HashMap<>();

    private static URLService ourInstance = new URLService();

    public static String JAR_LOCATION = System.getProperty("user.dir") + "/out/artifacts/csd_jar/csd.jar";

    static {
        getServerURLS();
    }

    private URLService() {
    }

    public static URLService getInstance() {
        return ourInstance;
    }

    public static List<URI> getAllServers() {
        return urls.values().stream()
                .map(url -> UriBuilder.fromUri("https://" + url.ip).port(url.port_rest).build())
                .collect(Collectors.toList());
    }

    public static URI getNextRestURL() {
        int curr = new Random().nextInt(urls.size());

        URLService.ServerData nextServer = urls.get(curr);

        if (nextServer == null) {
            throw new RuntimeException("Invalid Server: " + curr);
        }

        return UriBuilder.fromUri("https://" + nextServer.ip).port(nextServer.port_rest).build();
    }

    public static int getRestPort(int id) {
        URLService.ServerData sd = urls.get(id);

        return sd.port_rest;
    }

    public static int getNextURLSeq(int curr) {
        return (curr+1) % urls.size();
    }

    public static URI getURL(int id) {
        URLService.ServerData nextServer = urls.get(id);

        if (nextServer == null) {
            throw new RuntimeException("Invalid Server: " + id);
        }

        return UriBuilder.fromUri("https://" + nextServer.ip).port(nextServer.port_rest).build();
    }


    private static void getServerURLS() {
        Path path = Paths.get(CONFIG_HOME);

        try {
            Files.lines(path).forEachOrdered(line -> {
                        if (!line.startsWith("#") && !line.isEmpty()) {
                            String[] data = line.split(" ");

                            if (data.length != 4) {
                                throw new RuntimeException("Error loading servers address");
                            }

                            urls.put(Integer.parseInt(data[0]), new URLService.ServerData(data[1], data[2], data[3]));
                        }
                    }
            );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ServerData {
        String ip;
        int port_rest;
        int port_bft;

        ServerData(String ip, String port_bft, String port_rest) {
            this.ip = ip;
            this.port_rest = Integer.parseInt(port_rest);
            this.port_bft = Integer.parseInt(port_bft);
        }
    }
}
