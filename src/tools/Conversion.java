package tools;

import rest.reply.ReplyClass;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

public class Conversion {

    public static ReplyClass getReplyObject(byte[] reply) {
        ReplyClass rc;

        if (reply == null) {
            System.err.println("Null bft reply");
            return null;
        }

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);
             ObjectInput objIn = new ObjectInputStream(byteIn)) {

            rc = (ReplyClass) objIn.readObject();

        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error parsing bft reply : " + e);
            return null;
        }

        return rc;
    }
}
