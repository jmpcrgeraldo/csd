package container;

import hlib.hj.mlib.HelpSerial;
import hlib.hj.mlib.HomoAdd;
import hlib.hj.mlib.HomoOpeInt;
import hlib.hj.mlib.PaillierKey;
import tools.KeyHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import static container.SconeAPI.*;

@Path(SconeAPI.RESOURCE_PATH)
public class SconeServerHandler {
    private final String RSA_KEY;
    private ReentrantLock lock;

    public SconeServerHandler() {
        this.RSA_KEY = KeyHelper.loadPrv();
        lock = new ReentrantLock();
    }

    @GET
    @Path(HOMO_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    public Response homo_relation(@QueryParam(HOMO_VALUE_PARAM) BigInteger value,
                                  @QueryParam(HOMO_VALUE_CMP_PARAM) BigInteger cmp,
                                  @QueryParam(KEY_PARAM) String enc_user_key,
                                  @QueryParam(SYM_KEY_PARAM) String enc_sym_key) {

        System.out.println("GOT HOMO RELATION");

        if (value == null || cmp == null || enc_user_key == null || enc_sym_key == null) {
            System.err.println("Null Arguments at home_relation");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            String sym_key = KeyHelper.decryptSymKey(enc_sym_key, RSA_KEY);
            String user_key = KeyHelper.decryptUserKey(enc_user_key, sym_key);
            PaillierKey pk = (PaillierKey) HelpSerial.fromString(user_key);

            if (pk == null) {
                System.err.println("PaillierKey is null");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            BigInteger value_dec = HomoAdd.decrypt(value, pk);
            BigInteger cmp_dec = HomoAdd.decrypt(cmp, pk);

            return Response.status(Response.Status.OK).entity(value_dec.compareTo(cmp_dec)).build();
        } catch (Exception e) {
            System.err.println("Error decrypting");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path(HOMOS_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response homo_relations(EntryList entry) {

        System.out.println("GOT HOMO RELATIONS");

        BigInteger lower = entry.lower, upper = entry.upper;
        String enc_user_key = entry.key, enc_sym_key = entry.sym_key;
        List<Map.Entry<String, BigInteger>> entries = entry.entries;

        if (lower == null || upper == null || entries == null || enc_user_key == null || enc_sym_key == null) {
            System.err.println("Null Arguments at home_relation");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            String sym_key = KeyHelper.decryptSymKey(enc_sym_key, RSA_KEY);
            String user_key = KeyHelper.decryptUserKey(enc_user_key, sym_key);
            PaillierKey pk = (PaillierKey) HelpSerial.fromString(user_key);

            if (pk == null) {
                System.err.println("PaillierKey is null");
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            BigInteger lower_dec = HomoAdd.decrypt(lower, pk);
            BigInteger upper_dec = HomoAdd.decrypt(upper, pk);

            int l = lower_dec.intValue();
            int u = upper_dec.intValue();

            Iterator<Map.Entry<String, BigInteger>> it_k = entries.iterator();

            List<String> out_keys = new LinkedList<>();

            while (it_k.hasNext()) {
                Map.Entry<String, BigInteger> e = it_k.next();
                String current_key = e.getKey();
                BigInteger current_value = e.getValue();


                int value = HomoAdd.decrypt(current_value, pk).intValue();

                if (value >= l && value <= u)
                    out_keys.add(current_key);
            }

            return Response.status(Response.Status.OK).entity(out_keys).build();
        } catch (Exception e) {
            System.err.println("Error decrypting");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }

    @GET
    @Path(OPE_ADD_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ope_add(@QueryParam(OPE_VALUE_PARAM) Long value,
                            @QueryParam(OPE_VALUE_ADD_PARAM) Long add,
                            @QueryParam(KEY_PARAM) String enc_user_key,
                            @QueryParam(SYM_KEY_PARAM) String enc_sym_key) {

        System.out.println("GOT OPE ADD");

        if (value == null || add == null || enc_user_key == null || enc_sym_key == null) {
            System.err.println("Null Arguments at home_relation");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            String sym_key = KeyHelper.decryptSymKey(enc_sym_key, RSA_KEY);
            String user_key = KeyHelper.decryptUserKey(enc_user_key, sym_key);

            lock.lock();
            HomoOpeInt ope = new HomoOpeInt(user_key);

            Integer value_dec = ope.decrypt(value);
            Integer add_dec = ope.decrypt(add);
            lock.unlock();

            value_dec += add_dec;

            Long enc_val = ope.encrypt(value_dec);

            return Response.status(Response.Status.OK).entity(enc_val).build();
        } catch (Exception e) {
            System.err.println("Error decrypting");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path(OPE_SUB_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ope_sub(@QueryParam(OPE_VALUE_PARAM) Long value,
                            @QueryParam(OPE_VALUE_ADD_PARAM) Long sub,
                            @QueryParam(KEY_PARAM) String enc_user_key,
                            @QueryParam(SYM_KEY_PARAM) String enc_sym_key) {

        System.out.println("GOT OPE SUB");

        if (value == null || sub == null || enc_user_key == null || enc_sym_key == null) {
            System.err.println("Null Arguments at home_relation");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        try {
            String sym_key = KeyHelper.decryptSymKey(enc_sym_key, RSA_KEY);
            String user_key = KeyHelper.decryptUserKey(enc_user_key, sym_key);

            lock.lock();
            HomoOpeInt ope = new HomoOpeInt(user_key);
            Integer value_dec = ope.decrypt(value);
            Integer add_dec = ope.decrypt(sub);
            lock.unlock();

            value_dec -= add_dec;

            long enc_val = ope.encrypt(value_dec);

            return Response.status(Response.Status.OK).entity(enc_val).build();
        } catch (Exception e) {
            System.err.println("Error decrypting");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

}