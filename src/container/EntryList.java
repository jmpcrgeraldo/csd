package container;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public class EntryList implements Serializable {

    public BigInteger lower, upper;
    public String key, sym_key;
    public List<Map.Entry<String, BigInteger>> entries;

    public EntryList() {

    }

    public EntryList(BigInteger lower, BigInteger upper, String key, String sym_key, List<Map.Entry<String, BigInteger>> entries) {
        this.lower = lower;
        this.upper = upper;
        this.key = key;
        this.sym_key = sym_key;
        this.entries = entries;
    }
}

