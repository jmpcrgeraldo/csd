package container;

public interface SconeAPI {
    String SERVER_URI = "https://127.0.0.1/";
    int SERVER_PORT = 2233;

    String RESOURCE_PATH = "/scone";

    String HOMO_PATH = "/exec_homo_relation";
    String HOMOS_PATH = "/exec_homo_relations";
    String OPE_ADD_PATH = "/exec_ope_add";
    String OPE_SUB_PATH = "/exec_ope_sub";

    String HOMO_VALUE_PARAM = "homo_val1";
    String HOMO_VALUE_CMP_PARAM = "homo_val2";
    String OPE_VALUE_PARAM = "ope_val1";
    String OPE_VALUE_ADD_PARAM = "ope_val2";
    String KEY_PARAM = "enc_key";
    String SYM_KEY_PARAM = "enc_sym_key";
}
