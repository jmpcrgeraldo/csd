package container;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.security.NoSuchAlgorithmException;

public class SconeServer {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.setProperty("javax.net.ssl.keyStore", "jks/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "changeme");

        URI baseUri = UriBuilder.fromUri(SconeAPI.SERVER_URI).port(SconeAPI.SERVER_PORT).build();

        ResourceConfig config = new ResourceConfig();
        config.register(new SconeServerHandler());

        JdkHttpServerFactory.createHttpServer(baseUri, config, SSLContext.getDefault());

        System.out.println("Scone Server ready @ " + baseUri);
    }

}




