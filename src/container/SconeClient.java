package container;

import org.glassfish.jersey.client.ClientProperties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.math.BigInteger;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public class SconeClient {
    private static WebTarget target;



    static {
        new SconeClient();
    }

    private SconeClient() {
        System.setProperty("javax.net.ssl.trustStore", "jks/client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeme");

        Client client = ClientBuilder
                .newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .build();

        URI server = UriBuilder.fromUri(SconeAPI.SERVER_URI).port(SconeAPI.SERVER_PORT).build();
        target = client.target(server);
    }

    // -1 value < cmp ;; 0 v == c ;; 1 v > c
    public static int exec_homo_relation(BigInteger value, BigInteger cmp, String key, String sym_key) throws ErrorExc {

        Response resp = target
                .path(SconeAPI.RESOURCE_PATH + SconeAPI.HOMO_PATH)
                .queryParam(SconeAPI.HOMO_VALUE_PARAM, value)
                .queryParam(SconeAPI.HOMO_VALUE_CMP_PARAM, cmp)
                .queryParam(SconeAPI.KEY_PARAM, key)
                .queryParam(SconeAPI.SYM_KEY_PARAM, sym_key)
                .request(MediaType.APPLICATION_JSON).get();

        if (resp.getStatusInfo().equals(Response.Status.OK))
            return resp.readEntity(Integer.class);
        else
            throw new ErrorExc();
    }

    public static List<String> exec_homo_relations(BigInteger lower, BigInteger upper, List<Map.Entry<String, BigInteger>> entries, String key, String sym_key) {
        Response resp = target
                .path(SconeAPI.RESOURCE_PATH + SconeAPI.HOMOS_PATH)
                .request(MediaType.APPLICATION_JSON).post(Entity.json(
                        new EntryList(lower, upper, key, sym_key, entries)
                ));


        if (resp.getStatusInfo().equals(Response.Status.OK))
            return resp.readEntity(new GenericType<List<String>>() {
            });
        else
            return new LinkedList<>();
    }

    public static long exec_ope_add(Long value, Long add, String key, String sym_key) throws ErrorExc {
        Response resp = target
                .path(SconeAPI.RESOURCE_PATH + SconeAPI.OPE_ADD_PATH)
                .queryParam(SconeAPI.OPE_VALUE_PARAM, value)
                .queryParam(SconeAPI.OPE_VALUE_ADD_PARAM, add)
                .queryParam(SconeAPI.KEY_PARAM, key)
                .queryParam(SconeAPI.SYM_KEY_PARAM, sym_key)
                .request(MediaType.APPLICATION_JSON).get();

        if (resp.getStatusInfo().equals(Response.Status.OK))
            return resp.readEntity(Long.class);
        else
            throw new ErrorExc();
    }

    public static long exec_ope_sub(Long value, Long add, String key, String sym_key) throws ErrorExc {
        Response resp = target
                .path(SconeAPI.RESOURCE_PATH + SconeAPI.OPE_SUB_PATH)
                .queryParam(SconeAPI.OPE_VALUE_PARAM, value)
                .queryParam(SconeAPI.OPE_VALUE_ADD_PARAM, add)
                .queryParam(SconeAPI.KEY_PARAM, key)
                .queryParam(SconeAPI.SYM_KEY_PARAM, sym_key)
                .request(MediaType.APPLICATION_JSON).get();

        if (resp.getStatusInfo().equals(Response.Status.OK))
            return resp.readEntity(Long.class);
        else
            throw new ErrorExc();
    }

    static class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}


