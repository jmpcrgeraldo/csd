#!/bin/bash

exec="target/REST-VERSION1.0-jar-with-dependencies.jar"
view="target/config/currentView"

# Check server id is between 0 - 4
if (( $# != 1 || $1 > 4 || $1 < 0 )) ; then
	echo "Invalid Server ID $1"
    exit 1
fi

# Check if jar exists
if [ ! -f "$exec" ]
then
    echo "Jar not found, running maven package..."
    mvn clean package
fi

echo "Running Server Instance"
echo "ID: $1" 
echo "IP:" | tr "\n" " "; ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'

# Run Server Instance
rm -f "$view"
java -cp "$exec":lib/* rest.server.RESTServerInstance "$1"





# x-terminal-emulator -e "java -cp REST-VERSION1.0-jar-with-dependencies.jar:lib/* rest.server.RESTServerInstance 0 ; read line" &
# x-terminal-emulator -e "java -cp REST-VERSION1.0-jar-with-dependencies.jar:lib/* rest.server.RESTServerInstance 1 ; read line" &
# x-terminal-emulator -e "java -cp REST-VERSION1.0-jar-with-dependencies.jar:lib/* rest.server.RESTServerInstance 2 ; read line" &
# x-terminal-emulator -e "java -cp REST-VERSION1.0-jar-with-dependencies.jar:lib/* rest.server.RESTServerInstance 3 ; read line" &

# Run Client Instance
# x-terminal-emulator -e "java -cp REST-VERSION1.0-jar-with-dependencies.jar:lib/* rest.client.ClientInstance ; read line" &