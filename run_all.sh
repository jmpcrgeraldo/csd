#!/bin/bash

exec="REST-VERSION1.0-jar-with-dependencies.jar"
view="config/currentView"


# Run Server Instance
echo "Running All Servers"
cd target
rm -f "$view"
java -cp "$exec":lib/* rest.server.RESTServerInstance 0 &
java -cp "$exec":lib/* rest.server.RESTServerInstance 1 &
java -cp "$exec":lib/* rest.server.RESTServerInstance 2 &
java -cp "$exec":lib/* rest.server.RESTServerInstance 3 &